<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="author" content="" />
        <title>Trident Solutions ltd</title>
        <!-- ======================================================================== -->
        <link rel="icon" type="{{url('/')}}/assets/front/image/png" sizes="16x16" href="/favicon.ico">
        <!-- Bootstrap CSS -->
        <link href="{{url('/')}}/assets/front/css/bootstrap.css" rel="stylesheet" type="text/css" />
        <!--font-awesome-css-start-here-->
        <link href="{{url('/')}}/assets/front/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!--Custom Css-->
        <link href="{{url('/')}}/assets/front/css/trident.css" rel="stylesheet" type="text/css" />
        <link href="{{url('/')}}/assets/front/css/jquery.mCustomScrollbar.css" rel="stylesheet" type="text/css" />
        <!--Main JS-->
        <script type="text/javascript" src="{{url('/')}}/assets/front/js/jquery-1.11.3.min.js"></script>
        <!--[if lt IE 9]>-->
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <!--Sticky Menu-->
        <script type="text/javascript">
        $(document).ready(function() {
        var stickyNavTop = $('.header').offset().top;
        var stickyNav = function() {
        var scrollTop = $(window).scrollTop();
        if (scrollTop > stickyNavTop) {
        $('.header').addClass('sticky');
        } else {
        $('.header').removeClass('sticky');
        }
        };
        stickyNav();
        $(window).scroll(function() {
        stickyNav();
        });
        })
        </script>
        <!--Sticky Menu-->
        <!-- Min Top Menu Start Here  -->
        <script type="text/javascript">
        var doc_width = $(window).width();
        if (doc_width < 1180) {
        function openNav() {
        document.getElementById("mySidenav").style.width = "250px";
        $("body").css({
        "margin-left": "250px",
        "overflow-x": "hidden",
        "transition": "margin-left .5s",
        "position": "fixed"
        });
        $("#main").addClass("overlay");
        }
        function closeNav() {
        document.getElementById("mySidenav").style.width = "0";
        $("body").css({
        "margin-left": "0px",
        "transition": "margin-left .5s",
        "position": "relative"
        });
        $("#main").removeClass("overlay");
        }
        }
        $(".min-menu > li > .drop-block").click(function() {
        if (false == $(this).next().hasClass('menu-active')) {
        $('.sub-menu > ul').removeClass('menu-active');
        }
        $(this).next().toggleClass('menu-active');
        return false;
        });
        $("body").click(function() {
        $('.sub-menu > ul').removeClass('menu-active');
        });
        </script>
    </head>
    <body>
        <div id="main"></div>
        <!--Header section start here-->
        <header>
            <div class="header header-home">
                <div class="logo-block wow fadeInDown" data-wow-delay="0.2s">
                    <a href="index.html">
                        <img src="{{url('/')}}/assets/front/images/trident-solutions-itd.png" alt="" class="main-logo" />
                    </a>
                </div>
                <span class="menu-icon" onclick="openNav()">&#9776;</span>
                <!--Menu Start-->
                <div id="mySidenav" class="sidenav">
                    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
                    <div class="banner-img-block">
                        <img src="{{url('/')}}/assets/front/images/trident-solutions-itd.png" alt="Logo" />
                        <div class="img-responsive-logo"></div>
                    </div>
                    <ul class="min-menu">
                        <li><a href="javascript:void(0)" class="">Home</a></li>
                        <li><a href="javascript:void(0)" class="drop-block">Products</a></li>
                        <li><a href="javascript:void(0)" data-toggle="modal" data-target="#onload" class="drop-block">Contact Us</a></li>
                    </ul>
                    <div class="clr"></div>
                </div>
                <div class="clr"></div>
            </div>
            <!-- <div id="header-home"></div> -->
            <div class="blank-div"></div>
        </header>
        <!--Header section end here-->
        <!--banner section start-->
        <!-- <div class="banner">
            <div class="container">
                <div class="banner-text-block">
                    <h1>Build your business with <span>smart financing</span></h1>
                    <p>There are many variations of passages </p>
                    
                </div>
            </div>
        </div> -->
        <!--banner section end-->
        <!-- Solutions ltd Section Start -->
        <div class="product-section-col">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-4 col-md-6 col-lg-6">
                        <div class="image-section-img">
                            <img src="{{url('/')}}/assets/front/images/trident-logo-section.png" alt="" />
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-8 col-md-6 col-lg-6">
                        <div class="text-tridents">
                            <div class="title-solution">Solutions ltd</div>
                            <p>Trident Solutions Ltd., as part of FHS Group SA, is a trailblazer in the financial industry. Development of innovative financial products answering to the evolving needs of institutional clients to satisfy the needs of investors. A team of experts in product creation and cross-border regulation deploys products for FHS Group SA, institutional clients as well as retail investors. Swarm-intelligence, Crowd-investment strategies, total-return strategies, Securitization of investment opportunities outside of the financial industry and substitution solutions for fixed income bond products are at the focus of Trident Solutions` activites.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Solutions ltd Section End -->
        <!-- Product section Start -->
        <div class="solutions-ltd-section">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="product-sections-col-four">
                            <div class="image-part">
                                <img src="{{url('/')}}/assets/front/images/product-img1.jpg" alt="" />
                            </div>
                            <div class="texts-part">
                                <div class="title-tx">Dummy text of the printing </div>
                                <div class="p-tag">Lorem Ipsum is simply dummy text of the printing and typesetting industry.the printing and typesetting industry</div>
                                <a href="#" class="a-links-dm">Request More Info</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="product-sections-col-four">
                            <div class="image-part">
                                <img src="{{url('/')}}/assets/front/images/product-img2.jpg" alt="" />
                            </div>
                            <div class="texts-part">
                                <div class="title-tx">Dstablished fact that a reader</div>
                                <div class="p-tag">Lorem Ipsum is simply dummy text of the printing and typesetting industry.the printing and typesetting industry</div>
                                <a href="#" class="a-links-dm">Request More Info</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="product-sections-col-four">
                            <div class="image-part">
                                <img src="{{url('/')}}/assets/front/images/product-img3.jpg" alt="" />
                            </div>
                            <div class="texts-part">
                                <div class="title-tx">Variations of passages of Lorem</div>
                                <div class="p-tag">Lorem Ipsum is simply dummy text of the printing and typesetting industry.the printing and typesetting industry</div>
                                <a href="#" class="a-links-dm">Request More Info</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="more-button-section">
                            <a href="#" class="more-read">More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Product section End -->
        <!-- Contact Section Start -->
        <div class="contact-us-seccn">
            <div class="container">
                <div class="main-conts">
                    <div class="title-cont-us">Contact Us</div>
                    <div class="registered-clas">Registered Office</div>
                    <div class="address-tx">Prescient Asset Ltd. SICAV © <br>
                        16/1, Sandra Flats,<br>
                        Windsor Terrace,<br>
                        Sliema SLM 1858<br>
                    Malta</div>
                </div>
            </div>
            <div class="copyright-cls">
                Copyright  <i class="fa fa-copyright"></i> 2018 by <a href="#">Trident Solutions Ltd</a>. All Right Reserved.
            </div>
        </div>
        <!-- Contact Section End -->
        <!-- Term And conditions Modal Start -->
        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true"  id="onload">
            <div class="modal-dialog container-width">
                <button type="button" class="close btns-cls" data-dismiss="modal"><img src="{{url('/')}}/assets/front/images/cls-icn.png" alt="" /></button>
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="title-modal-red">Read Our <span>Terms &amp; Conditions</span></div>
                    <div class="txt-subtitles">Before you continue you have to agree to our Toc</div>
                    <div class="txt-p-sub">The following information is for your own protection and benefit. It explains certain legal and regulatory restrictions which apply to any investment in the products referred to in this website. After you have read and understood the following information, please click the ‘I AGREE’ button below to acknowledge that you have read and understood the information.</div>
                    <div class="main-content-section-mdl">
                        <div class="scroll-text-height content-d">
                            By accessing, browsing, and/or using this Website or linked websites and any pages hereof, you are indicating that you have read, acknowledged and agreed to be bound by the Terms of Use section.
                            <br><br>
                            This Website uses cookies to store information on your computer. By accessing this site, you consent to the placement of cookies on your machine as described in the Privacy Policy. If you do not consent to the use of cookies, please either disable cookies or refrain from accessing this site or any pages thereof.
                            <br><br>
                            Nothing contained on this Website constitutes tax, accounting, regulatory, legal, insurance or investment advice. Neither the information, nor any opinion, contained on this Website constitutes a solicitation or offer by Trident Solutions Ltd. ("TSL") or its affiliates to buy or sell any securities, futures, options or other financial instruments, nor shall any such security be offered or sold to any person in any jurisdiction in which such offer, solicitation, purchase, or sale would be unlawful under the securities laws of such jurisdiction. Decisions based on information contained on this Website are the sole responsibility of the visitor. In exchange for using this Website, the visitor agrees to indemnify and hold PAL, its officers, directors, employees, affiliates, agents, licensors and suppliers harmless against any and all claims, losses, liability, costs and expenses (including but not limited to attorneys' fees) arising from your use of this Website, from your violation of these Terms or from any decisions that the visitor makes based on such information.
                            <br><br>
                            The investments and strategies discussed in the Website may not be suitable for all investors and are not obligations of PAL or its affiliates or guaranteed by PAL or its affiliates. PAL makes no representations that the contents are appropriate for use in all locations, or that the transactions, securities, products, instruments, or services discussed on this site are available or appropriate for sale or use in all jurisdictions or countries, or by all investors or counterparties. By making available information on the Website, PAL does not represent that any investment vehicle is available or suitable for any particular user. All persons and entities accessing the Website agree to do so on their own initiative and are responsible for compliance with applicable local laws and regulations. All investments involve risk and may lose value. The value of your investment can go down depending upon market conditions. BEFORE ACQUIRING THE SHARES OF ANY INVESTMENT FUND BY PURCHASE OR EXCHANGE, IT IS YOUR RESPONSIBILITY TO READ THE FUND'S PROSPECTUS OR OFFERING MATERIALS.
                            <br><br>
                            This Website is for information purposes only and is not intended to be relied upon as a forecast, research or investment advice. The information on this Website does not constitute a recommendation, offer or solicitation to buy or sell any securities or to adopt any investment strategy. Although this material is based upon information that PAL considers reliable and endeavors to keep current, PAL does not assure that this material is accurate, current or complete, and it should not be relied upon as such. Any opinions expressed on this Website may change as subsequent conditions vary. Past performance is no guarantee of future results.
                            <br><br>
                            You expressly acknowledge that you have checked and confirm that you; (i) are legally entitled to view this website; (ii) understand that any products mentioned on the website may no longer be available for investment or may not be available to you; and (iii) understand that the information on the website is not a promotion and that you will not treat it as such and that any information on the website is not addressed specifically to you.
                        </div>
                    </div>
                    <div class="ftr-mdl"><a href="#" class="mldhide" data-toggle="modal" data-target="#SignUpModal"  class="i-agree-cls">Yes I Agree</a> <div class="p-footr"> No, I do not agree to the <a href="#" class="terms-a">Terms and Conditions</a></div></div>
                </div>
            </div>
        </div>
        <!-- Term And conditions Modal End -->
        
        
        <!-- Sign Up Modal Start -->
        <div id="SignUpModal" class="modal fade" role="dialog">
            <div class="modal-dialog container-width">
                <button type="button" class="close btns-cls" data-dismiss="modal"><img src="images/cls-icn.png" alt="" /></button>
                <!-- Modal content-->
                <form method="post" class="form-horizontal" id="frm_enquiry" action="{{url('/')}}/store">
                {{ csrf_field() }}
                <div class="modal-content">
                    <div class="title-modal-red"><span>Sign Up</span> Today</div>
                    <div class="txt-subtitles space-btn-asnr">Answer to the questions below and fill up the form to proceed.</div>
                    <div class="main-content-section-mdl">
                        <div class="scroll-text-height content-d">
                            <div class="space-btm-mrg">
                                <div class="radio-buttons">
                                    <div class="label-radio">I am a qualified investor</div>
                                    <div class="radio-btns" name="is_qualified">
                                        <div class="radio-btn">
                                            <input type="radio" id="f-option" name="is_qualified">
                                            <label for="f-option">Yes</label>
                                            <div class="check"></div>
                                        </div>
                                        <div class="radio-btn">
                                            <input type="radio" id="s-option" name="is_qualified">
                                            <label for="s-option">No</label>
                                            <div class="check"><div class="inside"></div></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="col-md-6">
                                    <div class="user-box1">
                                        <label class="label-l">First Name</label>
                                        <input class="cont-frm" placeholder="Enter Your First Name" type="text" name="first_name" required="" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="user-box1">
                                        <label class="label-l">Last Name</label>
                                        <input class="cont-frm" placeholder="Enter Your Last Name" type="text" name="last_name" required="" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="user-box1">
                                        <label class="label-l">Email Address</label>
                                        <input class="cont-frm" placeholder="Enter Your Email Address" type="email" name="email" required="" />
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="user-box1">
                                        <label class="label-l">Country of Residence</label>
                                        <div class="cont-frm arrow-down">
                                            <select class="frm-select" name="cmb_country" required="">
                                                <option>Select Country</option>
                                                <option>USA</option>
                                                <option>India</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="space-btm-mrg">
                                <div class="radio-buttons">
                                    <div class="label-radio">I wish to receive detailed information about the following product/s:</div>
                                    <div class="radio-btns">
                                        <div class="radio-btn">
                                            <input type="radio" id="d-option" name="receive_detail" value="YES">
                                            <label for="d-option">Yes</label>
                                            <div class="check"></div>
                                        </div>
                                        <div class="radio-btn">
                                            <input type="radio" id="e-option" name="receive_detail" value="NO">
                                            <label for="e-option">No</label>
                                            <div class="check"><div class="inside"></div></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="space-btm-mrg">
                                <div class="label-radio">Product Name</div>
                                <div class="col-md-4">
                                    <div class="check-box-inline">
                                        <input type="checkbox" class="css-checkbox" id="radio9" name="radiog_dark[]" value="Truegold" />
                                        <label class="css-label radGroup2" for="radio9">Truegold</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="check-box-inline">
                                        <input type="checkbox" checked="checked" class="css-checkbox" id="radio10" name="radiog_dark[]" value="Prescient VIX" />
                                        <label class="css-label radGroup2" for="radio10">Prescient VIX</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="check-box-inline">
                                        <input type="checkbox" checked="checked" class="css-checkbox" id="radio8" name="radiog_dark[]" value="Established fact Lorem" />
                                        <label class="css-label radGroup2" for="radio8">Established fact Lorem</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                           <!--  <div class="space-btm-mrg">
                                <div class="label-radio">Please enter your Name and email address</div>
                                <div class="col-md-6">
                                    <div class="user-box1">
                                        <label class="label-l">Full Name</label>
                                        <input class="cont-frm" placeholder="Enter Your Full Name" type="text"/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="user-box1">
                                        <label class="label-l">Email Address</label>
                                        <input class="cont-frm" placeholder="Enter Your Email Address" type="email" />
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="email-note">Name and E-mail must be filled out </div>
                                </div>
                            </div> -->
                        </div>
                    </div>
                    <div class="ftr-mdl">
                        {{-- <a href="#" class="confirm-estmodal">Confirm</a> --}}
                        <button type="submit" class="confirm-estmodal">Confirm</button>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <!-- Sign Up Modal End -->
        
        <!-- Min Top Menu Start End  -->
        <script>
        $( function() {
        $( ".mldhide" ).on( "click", function() {
        $( "#onload, .modal-backdrop" ).toggleClass( "addclass", 1000);
        });
        } );
        </script>
        
        <script>
        $(window).load(function(){
        $('#onload').modal('show');
        });
        </script>
        <!-- custom scrollbar plugin -->
        <script type="text/javascript" src="{{url('/')}}/assets/front/js/jquery.mCustomScrollbar.concat.min.js"></script>
        <script type="text/javascript">
        /*scrollbar start*/
        (function($){
        $(window).on("load",function(){
        $.mCustomScrollbar.defaults.scrollButtons.enable=true;
        $.mCustomScrollbar.defaults.axis="yx";
        $(".content-d").mCustomScrollbar({theme:"dark"});
        });
        })(jQuery);
        </script>
        <!--multi selection-->
        <script type="text/javascript" language="javascript" src="{{url('/')}}/assets/front/js/bootstrap.min.js"></script>
    </body>
</html>