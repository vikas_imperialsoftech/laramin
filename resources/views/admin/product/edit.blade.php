@extends('admin.layout.master')                

@section('main_content')
<!-- Page Content -->
  <div id="page-wrapper">
      <div class="container-fluid">
          <div class="row bg-title">
              <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <h4 class="page-title">{{$page_title or ''}}</h4> </div>
              <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                  <ol class="breadcrumb">
                      <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
                      <li><a href="{{$module_url_path}}">{{$module_title or ''}}</a></li>
                      <li class="active">{{$page_title or ''}}</li>
                  </ol>
              </div>
              <!-- /.col-lg-12 -->
          </div>
    <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                          @include('admin.layout._operation_status')
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">
                                    {!! Form::open([ 'url' => 'admin/product/save',
                                 'method'=>'POST',
                                 'enctype' =>'multipart/form-data',   
                                 'class'=>'form-horizontal',
                                 'id'=>'validation-form' 

                                ]) !!} 

                                  <input type="hidden" name="id" value="{{ $arr_product['id'] or false }}" />                   
                                       
                                        
                                    <div class="form-group row">
                                       <label class="col-2 col-form-label">Product Name<i class="red">*</i></label>
                                       <div class="col-10">
                                          <input type="text" class="form-control" name="name" value="{{ $arr_product['name'] }}" required="true"  placeholder="Enter Product name" />
                                          <span class="red">{{ $errors->first('name') }}</span>
                                       </div>
                                    </div>


                                       <div class="form-group row">
                                          <label class="col-2 col-form-label">Product Name<i class="red">*</i></label>
                                          <div class="col-10">
                                             <input type="text" class="form-control" name="description" value="{{$arr_product['description'] }}" required="true"  placeholder="Enter Product description" />
                                             <span class="red">{{ $errors->first('description') }}</span>
                                          </div>
                                       </div>


                                       {{--  <div class="form-group row">
                                          <label class="col-2 col-form-label" for="state"> Title <i class="red">*</i></label>
                                          <div class="col-10">
                                             {!! Form::text('title_en',$locale_category_title,['class'=>'form-control','data-parsley-required'=>'true', 'id'=>'title', 'placeholder'=>'Enter Title']) !!}
                                          </div>
                                        </div>
                                            <span>{{ $errors->first('title_en') }}</span> --}}

                                        <div class="form-group row">
                                           <label class="col-2 col-form-label" for="state">Image <i class="red">*</i></label>
                                            <div class="col-10">
                                              <input type="file" name="image" id="ad_image" class="dropify" data-default-file="{{$product_public_img_path.''.$arr_product['image']}}" />
                                            </div>
                                        </div>

                                        
                                           {{--    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" value="Update">Update</button> --}}
                                               <button type="button" class="btn btn-success waves-effect waves-light m-r-10" name="btn_update" id="btn_update" value="Update">Update</button>
                                               
                                             <a class="btn btn-inverse waves-effect waves-light" href="{{$module_url_path}}">Back</a>
                                        <!-- form-group -->
                                    {!! Form::close() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

<!-- END Main Content -->
<script type="text/javascript">
 $(document).ready(function(){
   
      $('#btn_update').click(function(){
      
      if($('#validation-form').parsley().validate() == false) return;

      $.ajax({
        url: '{{url('/admin/product/save')}}',
        type:"POST",
        data: new FormData($('#validation-form')[0]),
        contentType:false,
        processData:false,
        dataType:'json',
        success:function(data)
        {
          console.log(data);
          swal(data.status,data.description,data.status);
        }
      });
     
     });
   });
</script>
@stop