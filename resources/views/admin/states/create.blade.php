@extends('admin.layout.master')                
@section('main_content')
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">{{$page_title or ''}}</h4>
   </div>
   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
         <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
         <li><a href="{{$module_url_path}}">{{$module_title or ''}}</a></li>
         <li class="active">{{$page_title or ''}}</li>
      </ol>
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- .row -->
<div class="row">
   <div class="col-sm-12">
      <div class="white-box">
         @include('admin.layout._operation_status')
         <div class="tabbable">
            {!! Form::open([ 
                           'method'=>'POST',
                           'enctype' =>'multipart/form-data',   
                           'class'=>'form-horizontal', 
                           'id'=>'validation-form' 
            ]) !!} 
            {{ csrf_field() }}
            <ul  class="nav nav-tabs">
               @include('admin.layout._multi_lang_tab')
            </ul>
            <div  class="tab-content">
               @if(isset($arr_lang) && sizeof($arr_lang)>0)
               @foreach($arr_lang as $lang)
               <div class="tab-pane fade {{ $lang['locale']=='en'?'in active':'' }}" 
                  id="{{ $lang['locale'] }}">
                  @if($lang['locale']=="en")    
                  <div class="form-group row">
                     <label class="col-2 col-form-label" for="state"> Country Name<i class="red">*</i> </label>
                     <div class="col-10">

                       {{--  @if($lang['locale'] == 'en') 
                        {!!
                        Form::select('country_id', $arr_country,"",['class'=>'form-control','data-parsley-required'=>'true'])
                        !!}
                        @endif     --}}
                        <select name="country_id" class="form-control" id="country_id" required="">
                        <option selected disabled="disabled">-- select Country--</option>
                        <?php if($lang['locale'] == 'en') 
                        {
                          foreach ($arr_country as $countrykey => $countryvalue) 
                          {
                            ?>
                            <option>{{$countryvalue}}</option>
                            <?php
                          }
                       /* Form::select('country_id', $arr_country,"",['class'=>'form-control','data-parsley-required'=>'true'])
                        */}
                        ?>  
                        </select>
                     </div>
                     <span class='red'>{{ $errors->first('country_id_'.$lang['locale']) }}</span>  
                  </div>
                  @endif
                  <div class="form-group row">
                     <label class="col-2 col-form-label" for="state"> State Title <i class="red">*</i> </label>
                     <div class="col-10">
                        @if($lang['locale'] == 'en')        
                        {!! Form::text('state_title_en',old('state_title_en'),['class'=>'form-control','data-parsley-required'=>'true','placeholder'=>'State Title']) !!}
                        @else
                        {!! Form::text('state_title_'.$lang['locale'],old('state_title_'.$lang['locale'])) !!}
                        @endif    
                     </div>
                     <span class='red'>{{ $errors->first('state_title_'.$lang['locale']) }}</span>  
                  </div>
               </div>
               @endforeach
               @endif
            </div>
            <div class="form-group row">
               <div class="col-10">
                  {{-- <button class="btn btn-success waves-effect waves-light m-r-10" type="submit" name="Save" value="true"> Save</button> --}}
                  <button class="btn btn-success waves-effect waves-light m-r-10" type="button" id="btn_add" name="Save" value="true"> Save</button>
                  <a class="btn btn-inverse waves-effect waves-light" href="{{$module_url_path}}">Back</a>
               </div>
            </div>
            {!! Form::close() !!}
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script type="text/javascript">
    $(document).ready(function(){

      $('#btn_add').click(function(){

       if($('#validation-form').parsley().validate() == false) return ;
       
       var formdata = $('#validation-form').serialize();

        $.post('{{url('admin/states/save')}}',formdata,function(data,status){

         if('success' == data.status)
         {
            $('#validation-form')[0].reset();
            
            swal({
                  title: data.status,
                  text: data.description,
                  type: data.status,
                  confirmButtonText: "OK",
                  closeOnConfirm: false
                },
                function(isConfirm,tmp)
                {
                  if(isConfirm==true)
                  {
                    window.location = data.link;
                  }
                });
             }
             else
             {
                swal(data.status,data.description,data.status);
             }
         });
      }); 
   });
</script>
@stop