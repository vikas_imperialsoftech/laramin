@extends('admin.layout.master')                
@section('main_content')
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">{{$page_title or ''}}</h4>
   </div>
   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
         <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
         <li><a href="{{$module_url_path}}">{{$module_title or ''}}</a></li>
         <li class="active">{{$page_title or ''}}</li>
      </ol>
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- .row -->
<div class="row">
<div class="col-sm-12">
<div class="white-box">
   @include('admin.layout._operation_status')
   <div class="row">
      <div class="col-sm-12 col-xs-12">
         <form method="POST" id="validation-form" class="form-horizontal" action="{{$module_url_path}}/update" enctype="multipart/form-data">
            {{ csrf_field() }} 

            @if(isset($arr_data) && sizeof($arr_data)>0)
            <div class="form-group row">
               <label class="col-2 col-form-label" >Keyword </label>
               <div class="col-10"><input name="keyword" readonly="" data-parsley-required="true" value="{{isset($keyword) ? $keyword : ''}}" class="form-control"/></div>
            </div>

            @foreach($arr_data as $key => $data)   
            {{-- {{dd($arr_data)}}  --}}
            
            <input type="hidden" name="keyword_arr[{{$key}}][id]" value="{{isset($data['id']) ? $data['id'] : ''}}"> 
            <div class="form-group row">
               <label class="col-2 col-form-label" >Title ({{isset($data['locale']) ? $data['locale'] : ''}})  <i class="red">*</i></label>
               <div class="col-10">
                  <input type="text" name="keyword_arr[{{$key}}][title]" data-parsley-required="true" placeholder="Enter Module Title" value="{{isset($data['title']) ? $data['title'] : ''}}" class="form-control"/>
               </div>
               <span class='red'>{{ $errors->first($data['title']) }}</span>  
            </div>
            @endforeach
            @endif                
            <div class="form-group row">
               <div class="col-10">
                  <button type="submit" class="btn btn-success waves-effect waves-light" value="Update">Update</button>
                  <a class="btn btn-inverse waves-effect waves-light" href="{{$module_url_path}}">Back</a>
               </div>
            </div>
         </form>
      </div>
   </div>
</div>
</div>
</div>
<!-- END Main Content --> 
<script type="text/javascript">
   $(document).ready(function()
   {  
     $("#autocomplete").bind('keypress',function(event)
     {
     });
   }); 
   
</script>
@endsection