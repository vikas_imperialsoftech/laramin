@extends('admin.layout.master')                
@section('main_content')
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">{{$page_title or ''}}</h4>
   </div>
   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
         <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
         <li><a href="{{$module_url_path}}">{{$module_title or ''}}</a></li>
         <li class="active">{{$page_title or ''}}</li>
      </ol>
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- .row -->
<div class="row">
   <div class="col-sm-12">
      <div class="white-box">
         @include('admin.layout._operation_status')
         <div class="row">
            <div class="col-sm-12 col-xs-12">
               {!! Form::open([ 
                              'method'=>'POST',   
                              'class'=>'form-horizontal', 
                              'id'=>'validation-form' 
               ]) !!} 
               <ul  class="nav nav-tabs">
                  @include('admin.layout._multi_lang_tab')
                  
               </ul>
               <div  class="tab-content">
                  @if(isset($arr_lang) && sizeof($arr_lang)>0)
                  @foreach($arr_lang as $lang)
                  <div class="tab-pane fade {{ $lang['locale']=='en'?'in active':'' }}" 
                     id="{{ $lang['locale'] }}">
                     <div class="form-group row">
                        <label class="col-2 col-form-label" for="state"> Question @if($lang['locale'] == 'en') 
                        <i class="red">*</i>
                        @endif
                        </label>
                        <div class="col-sm-6 col-lg-8 controls">
                           @if($lang['locale'] == 'en')        
                           {!! Form::text('question_'.$lang['locale'],old('question_'.$lang['locale']),['class'=>'form-control','data-parsley-required'=>'true','data-parsley-maxlength'=>'500','data-parsley-minlength'=>'10', 'placeholder'=>'Question']) !!}
                           @else
                           {!! Form::text('question_'.$lang['locale'],old('question_'.$lang['locale'])) !!}
                           @endif    
                        </div>
                        <span class='red'>{{ $errors->first('question_'.$lang['locale']) }}</span>  
                     </div>
                     <div class="form-group row">
                        <label class="col-2 col-form-label" for="state"> Answer 
                        @if($lang['locale'] == 'en') 
                        <i class="red">*</i>
                        @endif
                        </label>
                        <div class="col-sm-6 col-lg-8 controls">
                           @if($lang['locale'] == 'en')        
                           {!! Form::textarea('answer_'.$lang['locale'],old('answer_'.$lang['locale']),['class'=>'form-control','rows'=>'20', 'id'=>'answer_'.$lang['locale'], 'placeholder'=>'Answer','required']) !!}
                           @else
                           {!! Form::textarea('answer_'.$lang['locale'],old('answer_'.$lang['locale'])) !!}
                           @endif    
                        </div>
                        <span class='red'>{{ $errors->first('answer_'.$lang['locale']) }}</span>  
                     </div>
                  </div>
                  @endforeach
                  @endif
               </div>
               <br>
               <div class="form-group">
                  <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                    {{-- <button class="btn btn-success waves-effect waves-light m-r-10" type="submit" name="Save" value="true"> Save</button> --}}
                    <button class="btn btn-success waves-effect waves-light m-r-10" onclick="saveTinyMceContent();" type="button" name="Save" id="btn_add" value="true"> Save</button>
                  <a class="btn btn-inverse waves-effect waves-light" href="{{$module_url_path}}">Back</a>
                  </div>

               </div>
               {!! Form::close() !!}
            </div>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script type="text/javascript" src="{{url('/assets/js/tinyMCE.js')}}"></script>
<script type="text/javascript">

   $(document).ready(function(){

     // CKEDITOR.replace('answer_{{$lang['locale']}}');

       $('#btn_add').click(function(){

         if($('#validation-form').parsley().validate()==false) return;
         
         var formdata = $('#validation-form').serialize();

         $.post('{{url('/admin/faq/save')}}',formdata,function(data){
            
         if('success' == data.status)
         {
           $('#validation-form')[0].reset();

           swal({
               title: data.status,
               text: data.description,
               type: data.status,
               confirmButtonText: "OK",
               closeOnConfirm: false
             },
             function(isConfirm,tmp)
             {
               if(isConfirm==true)
               {
                 window.location = data.link;
               }
             });
          }
          else
          {
            swal(data.status,data.description,data.status);
          }
         });
        });
   });

   $("textarea").on('keyup',function()
   {
      saveTinyMceContent();
   });

   function saveTinyMceContent()
   {
     tinyMCE.triggerSave();
   }
  
</script>
@stop