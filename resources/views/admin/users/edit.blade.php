@extends('admin.layout.master')                

@section('main_content')
<!-- Page Content -->
  <div id="page-wrapper">
      <div class="container-fluid">
          <div class="row bg-title">
              <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <h4 class="page-title">{{$page_title or ''}}</h4> </div>
              <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                  <ol class="breadcrumb">
                      <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
                      <li><a href="{{$module_url_path}}">{{$module_title or ''}}</a></li>
                      <li class="active">{{$page_title or ''}}</li>
                  </ol>
              </div>
              <!-- /.col-lg-12 -->
          </div>
    <!-- .row -->
                <div class="row">
                    <div class="col-sm-12">
                        <div class="white-box">
                          @include('admin.layout._operation_status')
                            <div class="row">
                                <div class="col-sm-12 col-xs-12">

           {!! Form::open([ 'url' => $module_url_path.'/save',
                                 'method'=>'POST',
                                 'enctype' =>'multipart/form-data',   
                                 'class'=>'form-horizontal', 
                                 'id'=>'validation-form' 
                                ]) !!} 

           {{ csrf_field() }}

           @if(isset($arr_data) && count($arr_data) > 0)   

           {!! Form::hidden('user_id',isset($arr_data['id']) ? $arr_data['id']: "")!!}

            <div class="form-group row">
              <label class="col-2 col-form-label" for="state"> Firstname <i class="red">*</i></label>
              <div class="col-10">
                  {!! Form::text('first_name',isset($arr_data['first_name']) ? $arr_data['first_name']: "",['class'=>'form-control','data-parsley-required'=>'true', 'placeholder'=>'Enter Firstname']) !!}
              </div>
                <span class="red">{{ $errors->first('first_name') }}</span>
            </div>

            <div class="form-group row">
              <label class="col-2 col-form-label" for="state"> Lastname <i class="red">*</i></label>
              <div class="col-10">
                  {!! Form::text('last_name',isset($arr_data['last_name']) ? $arr_data['last_name']: "",['class'=>'form-control','data-parsley-required'=>'true', 'placeholder'=>'Enter Lastname']) !!}
              </div>
                <span class="red">{{ $errors->first('last_name') }}</span>
            </div>

            <div class="form-group row">
              <label class="col-2 col-form-label" for="state"> Email <i class="red">*</i></label>
              <div class="col-10">
                  {!! Form::text('email',isset($arr_data['email']) ? $arr_data['email']: "",['class'=>'form-control','data-parsley-required'=>'true', 'placeholder'=>'Enter Email']) !!}
              </div>
                <span class="red">{{ $errors->first('email') }}</span>
            </div>

            <div class="form-group row">
              <label class="col-2 col-form-label" for="state"> Phone <i class="red">*</i></label>
              <div class="col-10">
                  {!! Form::text('text',isset($arr_data['phone']) ? $arr_data['phone']: "",['class'=>'form-control','data-parsley-required'=>'true','data-parsley-type'=>'digits', 'placeholder'=>'Enter Phone No','name'=>'phone']) !!}
              </div>
                <span class="red">{{ $errors->first('phone') }}</span>
            </div>

            <div class="form-group row">
                <label class="col-2 col-form-label">Country<i class="red">*</i></label>
                <div class="col-10" >

                     <select name="country" id="country" onchange="getStates()" class="form-control">
                         
                        @foreach($arr_country as $countriesKey => $countriesValues)
                         
                           <option Autocompleteclass="form-control" <?php if(isset($arr_data['country_name']) &&  $countriesValues['id'] == $arr_data['country_name']) {echo 'selected=selected'; }  ?> value="{{$countriesValues['id']}}"  >{{$countriesValues['country_name']}}</option>
                
                        @endforeach

                   </select>
                    <span class="help-block">{{ $errors->first('country') }}</span>
                </div>
            </div>

            <div class="form-group row">
                  <label class="col-2 col-form-label">Street Address<i class="red">*</i></label>
                  <div class="col-10" >
                      {!! Form::text('street_address',isset($arr_data['street_address']) ? $arr_data['street_address']: "",['class'=>'form-control','data-parsley-required'=>'true', 'placeholder'=>'Enter Street Address', 'id'=>"autocomplete"]) !!}
                      <span class="help-block">{{ $errors->first('street_address') }}</span>
                  </div>
            </div>

            <div class="form-group row">
                  <label class="col-2 col-form-label">State</label>
                  <div class="col-10" >
                      <select name="state" id="state" onchange="getCity()" class="form-control">
                                          
                       {{-- <option selected disabled>--select--</option> --}}
                            @foreach($states as $statesKey => $statesValues)
                             
                               <option class="form-control" <?php if(isset($arr_data['state']) &&  $statesValues['id'] == $arr_data['state']) {echo 'selected=selected'; }  ?> value="{{$statesValues['id']}}"  >{{$statesValues['name']}}</option>

                            @endforeach

                       </select>

                      <span class="help-block">{{ $errors->first('state') }}</span>
                  </div>
            </div>

           {{--  <div class="form-group row">
                  <label class="col-2 col-form-label">City</label>
                  <div class="col-10" >
                     {!! Form::text('city',isset($arr_data['city']) ? $arr_data['city']: "",['class'=>'form-control','placeholder'=>'City', 'id'=>"locality" ,'readonly'=>'']) !!}
                      <span class="help-block">{{ $errors->first('city') }}</span>
                  </div>
            </div> --}}

            <div class="form-group row">
                  <label class="col-2 col-form-label">Zip Code</label>
                  <div class="col-10" >
                     {!! Form::text('zipcode',isset($arr_data['zipcode']) ? $arr_data['zipcode']: "",['class'=>'form-control','placeholder'=>'Zip Code', 'id'=>"postal_code",'data-parsley-type'=>'number','required','data-parsley-length'=>'[4, 12]']) !!}
                      <span class="help-block">{{ $errors->first('zipcode') }}</span>
                  </div>
            </div>
             <div class="form-group row">
                  <label class="col-2 col-form-label">Password<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="password" class="form-control" name="password" placeholder="Enter password" data-parsley-required="true" data-parsley-length="[6, 16]" value="{{ old('password') }}" />
                     <span class="red">{{ $errors->first('password') }}</span>
                  </div>
               </div>
            
            <div class="form-group row">
               <label class="col-2 col-form-label" for="state">Profile Image <i class="red">*</i></label>
                <div class="col-10">
                  <input type="file" name="profile_image" id="profile_image" class="dropify" data-default-file="{{ $user_profile_public_img_path.$arr_data['profile_image']}}" />
                </div>
            </div>

            <div class="form-group row">
              <div class="col-10">
               {{--  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" value="Update">Update</button> --}}
                <button type="button" class="btn btn-success waves-effect waves-light m-r-10" value="Update" id="btn_update">Update</button>
                <a class="btn btn-inverse waves-effect waves-light" href="{{$module_url_path}}">Back</a>
              </div>
            </div>

            @else 
              <div class="form-group row">
                <div class="col-10">
                  <h3><strong>No Record found..</strong></h3>     
                </div>
              </div>
            @endif
    
          {!! Form::close() !!}
      </div>
    </div>
  </div>
 </div>
 </div> 
  <!-- END Main Content -->

<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCccvQtzVx4aAt05YnfzJDSWEzPiVnNVsY&libraries=places&callback=initAutocomplete"
        async defer>
</script>

<script>  

$(document).ready(function () {
    // changeCountryRestriction('#country');
     $('#btn_update').click(function(){
      
      if($('#validation-form').parsley().validate() == false) return;

      $.ajax({
        url: '{{url('/admin/users/save')}}',
        type:"POST",
        data: new FormData($('#validation-form')[0]),
        contentType:false,
        processData:false,
        dataType:'json',
        success:function(data)
        {
          console.log(data);
          swal(data.status,data.description,data.status);
        }
      });
     
     });
});


  var glob_autocomplete;
  var glob_component_form = 
  {
    street_number: 'short_name',
    route: 'long_name',
    locality: 'long_name',
    administrative_area_level_1: 'long_name',
    postal_code: 'short_name'
  };

  var glob_options = {};
  glob_options.types = ['address'];

  function changeCountryRestriction(ref)
  {
    var country_code = $(ref).val();
    destroyPlaceChangeListener(autocomplete);
    // load states function
    // loadStates(country_code);  

    glob_options.componentRestrictions = {country: country_code}; 

    initAutocomplete(country_code);

    glob_autocomplete = false;
    glob_autocomplete = initGoogleAutoComponent($('#autocomplete')[0],glob_options,glob_autocomplete);
  }


  function initAutocomplete(country_code) 
  {
    glob_options.componentRestrictions = {country: country_code}; 

    glob_autocomplete = false;
    glob_autocomplete = initGoogleAutoComponent($('#autocomplete')[0],glob_options,glob_autocomplete);
  }


  function initGoogleAutoComponent(elem,options,autocomplete_ref)
  {
    autocomplete_ref = new google.maps.places.Autocomplete(elem,options);
    autocomplete_ref = createPlaceChangeListener(autocomplete_ref,fillInAddress);

    return autocomplete_ref;
  }
  

  function createPlaceChangeListener(autocomplete_ref,fillInAddress)
  {
    autocomplete_ref.addListener('place_changed', fillInAddress);
    return autocomplete_ref;
  }

  function destroyPlaceChangeListener(autocomplete_ref)
  {
    google.maps.event.clearInstanceListeners(autocomplete_ref);
  }

  function fillInAddress() 
  {
    // Get the place details from the autocomplete object.
    var place = glob_autocomplete.getPlace();
    console.log(place)  ;
    for (var component in glob_component_form) 
    {
        $("#"+component).val("");
        $("#"+component).attr('disabled',false);
    }
    
    if(place.address_components.length > 0 )
    {
      $.each(place.address_components,function(index,elem)
      {
          var addressType = elem.types[0];
          if(glob_component_form[addressType])
          {
            var val = elem[glob_component_form[addressType]];
            $("#"+addressType).val(val) ;  
          }
      });  
    }
  }

  function getStates()
  {
     var site_url = "{{ url('/') }}";

     var country_id = $('#country').val();

      $.ajax({
                url       : site_url+'/admin/users/get_states/'+country_id,
                type      : "GET",
                dataType  : "JSON",
                success:function(data)
                {
                  // console.log(data);
                  var html = "";
                  $(data).each(function(index,val){ 
                     html+="<option value='"+val['id']+"'>"+val['name']+"</option>";
                  });

                  $("#state").html(html);
                }
            });
      
  }

</script>     
@stop                    
