@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">{{$module_title or ''}}</h4>
   </div>
   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
         <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
         <li class="active">{{$module_title or ''}}</li>
      </ol>
   </div>
   <!-- /.col-lg-12 -->
</div>
<div class="row">
   <div class="col-sm-12">
      <div class="white-box">
         <div class="row">
            <div class="col-md-9 user-profile-info">
               <p><span>City Name:</span>{{ isset($arr_data['city_title'])?$arr_data['city_title']:'' }}</p>
               <p><span>State Name:</span> {{ isset($arr_data['state_details']['state_title'])?$arr_data['state_details']['state_title']:'-' }}</p>
               <p><span>Country Name:</span> {{ isset($arr_data['country_details']['country_name'])?$arr_data['country_details']['country_name']:'' }}</p>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
@stop