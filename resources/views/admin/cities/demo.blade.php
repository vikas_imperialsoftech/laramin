@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">{{$module_title or ''}}</h4>
   </div>
   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
         <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
         <li class="active">{{$module_title or ''}}</li>
      </ol>
   </div>
   <!-- /.col-lg-12 -->
</div>
<div class="row">
   <div class="col-sm-12">
      <div class="white-box">
         @include('admin.layout._operation_status') 
          {!! Form::open([ 'url' => $module_url_path.'/multi_action',
                                 'method'=>'POST',
                                 'enctype' =>'multipart/form-data',   
                                 'class'=>'form-horizontal', 
                                 'id'=>'frm_manage' 
                                ]) !!}

            {{ csrf_field() }}
          <div class="pull-right">

          <a href="{{ url($module_url_path.'/create') }}" class="btn btn-outline btn-info btn-circle show-tooltip" title="Add More"><i class="fa fa-plus"></i> </a> 
            <a href="javascript:void(0);" onclick="javascript : return check_multi_action('checked_record[]','frm_manage','activate');" class="btn btn-circle btn-success btn-outline show-tooltip" title="Multiple Unlock"><i class="ti-unlock"></i></a> 
            <a  href="javascript:void(0);" onclick="javascript : return check_multi_action('checked_record[]','frm_manage','deactivate');" class="btn btn-circle btn-danger btn-outline show-tooltip" title="Multiple Lock"><i class="ti-lock"></i> </a> 
            <a  href="javascript:void(0);" onclick="javascript : return check_multi_action('checked_record[]','frm_manage','delete');" class="btn btn-circle btn-danger btn-outline show-tooltip" title="Multiple Delete"><i class="ti-trash"></i> </a> 
            <a href="javascript:void(0)" onclick="javascript:location.reload();" class="btn btn-outline btn-info btn-circle show-tooltip" title="Refresh"><i class="fa fa-refresh"></i> </a>
            
          </div>
          <br/>
          <br>
          <div class="clearfix"></div>
          <div class="table-responsive" style="border:0">

            <input type="hidden" name="multi_action" value="" />

            <table class="table table-advance"  id="table1" >
              <thead>
                <tr>
                  <th>
                    <div class="checkbox checkbox-success">
                       <input class="checkboxInputAll" id="checkbox0" type="checkbox">
                       <label for="checkbox0">  </label>
                    </div>
                  </th>
                  <th>City</th> 
                  <th>State/Region :: Country</th>
                  <th>Status</th>
                  <th style="text-align:center;">Translations</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
          
                @if(isset($arr_data) && sizeof($arr_data)>0)
                  @foreach($arr_data as $data)

                  <tr>
                    <td> 
                      <div class="checkbox checkbox-success"><input type="checkbox" name="checked_record[]" value="{{base64_encode($data['id'])}}" id="checkbox{{$data['id']}}" class="case checkboxInput"/><label for="checkbox{{$data['id']}}"></label> </div>
                    </td>
                    
                    <td > {{ $data['city_title'] or '' }} </td> 
                    
                    <td > 
                    {{ isset($data['state_details']['state_title'])?$data['state_details']['state_title'].'::':''}}  
                    {{ $data['country_details']['country_name'] or ''}} </td> 

                    <td>
                      @if($data['is_active']==1)
                        <input type="checkbox" checked data-size="small"  data-enc_id="{{base64_encode($data['id'])}}"  id="status_{{$data['id']}}" class="js-switch toggleSwitch" data-type="deactivate" data-color="#99d683" data-secondary-color="#f96262" />
                      @else
                        <input type="checkbox" data-size="small" data-enc_id="{{base64_encode($data['id'])}}"  class="js-switch toggleSwitch" data-type="activate" data-color="#99d683" data-secondary-color="#f96262" />
                      @endif
                     
                    </td>

                    <!-- Translations  -->
                    <td style="text-align:center;">

                       <button type="button" class="btn btn-circle btn-primary btn-outline show-tooltip" data-toggle="modal" data-target="#see_avail_translation_{{ $data['id']}}"  title="View Translations"><i class="ti-eye"></i></button>

                        <!-- Modal -->
                        <div id="see_avail_translation_{{ $data['id']}}" class="modal fade" role="dialog">
                          <div class="modal-dialog modal-sm">

                            <!-- Modal content-->
                            <div class="modal-content">
                              <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                <h4 class="modal-title">Translation Available In</h4>
                              </div>
                              <div class="modal-body">
                                   @if(isset($data['arr_translation_status']) && sizeof($data['arr_translation_status'])>0)
                                    <ul style="list-style-type: none;">
                                      @foreach($data['arr_translation_status'] as $translation_status)
                                        @if($translation_status['is_avail']==1)
                                          <li>
                                            <h5>
                                              <i class="fa fa-check text-success"></i>
                                              {{ $translation_status['title'] }}
                                            </h5>
                                          </li>
                                        @else
                                          <li>
                                            <h5>
                                              <i class="fa fa-times text-danger"></i> 
                                              {{ $translation_status['title'] }}
                                            </h5>  
                                          </li>
                                        @endif       
                                      @endforeach
                                    </ul>
                                   @endif
                              </div>
                              <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                              </div>
                            </div>

                          </div>
                        </div>

                    </td> 
                    
                    <td> 
                      <a class="btn btn-circle btn-success btn-outline show-tooltip" href="{{ $module_url_path.'/show/'.base64_encode($data['id']) }}" 
                         title="view" >
                          <i class="ti-eye" > </i>
                        </a>
                       

                        <a class="btn btn-outline btn-info btn-circle show-tooltip" href="{{ $module_url_path.'/edit/'.base64_encode($data['id']) }}" title="Edit">
                          <i class="ti-pencil" ></i>
                        </a>  
                     
                     <a class="btn btn-outline btn-danger btn-circle show-tooltip" title="delete" href="{{ $module_url_path.'/delete/'.base64_encode($data['id'])}}" onclick="return confirm_action(this,event,'Are you sure to delete this record?')" >
                          <i class="ti-trash" ></i>  
                    </td>
                  </tr>
                  @endforeach
                @endif
                 
              </tbody>
            </table>
          </div>
        <div> </div>
         
          {!! Form::close() !!}
      </div>
  </div>
</div>

<!-- END Main Content -->
<script type="text/javascript">
  var module_url_path = '{{$module_url_path}}';
    function show_details(url)
    { 
       
        window.location.href = url;
    } 
    $(function(){
     $("input.checkboxInputAll").click(function(){

        if($("input.checkboxInput:checkbox:checked").length <= 0){
            $("input.checkboxInput").prop('checked',true);
        }else{
            $("input.checkboxInput").prop('checked',false);
        }

       }); 
    });
    function statusChange(data)
    {
      var ref = data; 
      var type = data.attr('data-type');
      var enc_id = data.attr('data-enc_id');
      var id = data.attr('data-id');

        $.ajax({
          url:module_url_path+'/'+type,
          type:'GET',
          data:{id:enc_id},
          dataType:'json',
          success: function(response)
          {
            if(response.status=='SUCCESS'){
              if(response.data=='ACTIVE')
              {
                $(ref)[0].checked = true;  
                $(ref).attr('data-type','deactivate');

              }else
              {
                $(ref)[0].checked = false;  
                $(ref).attr('data-type','activate');
              }
            }
            else
            {
              sweetAlert('Error','Something went wrong!','error');
            }  
          }
        });  
    }
</script>

@stop                    


