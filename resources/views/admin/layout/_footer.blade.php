     <!-- /.container-fluid -->
            <footer class="footer text-center"> {{date('Y')}} &copy; {{config('app.project.name')}} Admin  </footer>
            
<!-- jQuery -->

    <script src="{{url('/')}}/assets/plugins/bower_components/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="{{url('/')}}/assets/bootstrap/dist/js/tether.min.js"></script>
    <script src="{{url('/')}}/assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="{{url('/')}}/assets/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="{{url('/')}}/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js"></script>
    <!--slimscroll JavaScript -->
    <script src="{{url('/')}}/assets/js/jquery.slimscroll.js"></script>
    <!--Wave Effects -->
    <script src="{{url('/')}}/assets/js/waves.js"></script>
    <!--Counter js -->
    <script src="{{url('/')}}/assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js"></script>
    <script src="{{url('/')}}/assets/plugins/bower_components/counterup/jquery.counterup.min.js"></script>
    <!--Morris JavaScript -->
    <script src="{{url('/')}}/assets/plugins/bower_components/raphael/raphael-min.js"></script>
    {{-- <script src="{{url('/')}}/assets/plugins/bower_components/morrisjs/morris.js"></script> --}}
    <!-- Custom Theme JavaScript -->
    <script src="{{url('/')}}/assets/js/custom.min.js"></script>
    <script src="{{url('/')}}/assets/js/dashboard1.js"></script>
    <!-- Sparkline chart JavaScript -->
    <script src="{{url('/')}}/assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js"></script>
    <script src="{{url('/')}}/assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js"></script>
    <script src="{{url('/')}}/assets/plugins/bower_components/toast-master/js/jquery.toast.js"></script>

    <!-- data table js-->
    <script type="text/javascript" src="{{url('/')}}/assets/data-tables/latest/jquery.dataTables.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/data-tables/latest/dataTables.bootstrap.min.js"></script>
    
    <script type="text/javascript" src="{{ url('/') }}/assets/js/jquery-validation/dist/jquery.validate.min.js"></script>
    <script type="text/javascript" src="{{ url('/') }}/assets/js/jquery-validation/dist/additional-methods.js"></script>
    <script src="{{ url('/') }}/assets/js/validation.js"></script>

    <script type="text/javascript" src="{{url('/')}}/assets/js/sweetalert_msg.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/sweetalert/sweetalert.js"></script>

    <script type="text/javascript" src="{{url('/')}}/assets/js/Parsley/dist/parsley.min.js"></script>
    

    <script src="{{url('/')}}/assets/plugins/bower_components/dropify/dist/js/dropify.min.js"></script>

    <!--Switechery js-->
    <script src="{{url('/')}}/assets/plugins/bower_components/switchery/dist/switchery.min.js"></script>

     <script type="text/javascript" src="{{url('/')}}/assets/data-tables/latest/dataTables.bootstrap.min.js"></script>
    <script type="text/javascript" src="{{url('/')}}/assets/data-tables/latest/dataTables.responsive.min.js"></script>
    
    <!-- Tinymce editor js-->
    {{-- <script src="{{url('/')}}/assets/js/tinymce.min.js"></script> --}}
    <script type="text/javascript" src="{{url('/assets/js/tinyMCE.js')}}"></script>
    <!-- Tinymce editor js-->
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    {{-- <script src="{{'/'}}/ckeditor.js"></script> --}}
    <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();
        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });
        // Used events
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });
        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });
        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        });

    });
    </script>
    
    <script type="text/javascript">
          $(document).ready(function(){
            // $('#validation-form').validate();
            // $('#validation-form').parsley();
          });

        var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
        $('.js-switch').each(function() {
         new Switchery($(this)[0], $(this).data());
        });

        $("input.toggleSwitch").change(function(){
        statusChange($(this));
        });
        
    </script>

     <!--Style Switcher -->
    <script src="{{url('/')}}/assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!--Style Switcher -->
    <script src="{{url('/')}}/assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"></script>
    <!--Parsley Js-->
    <script type="text/javascript" src="{{url('/')}}/assets/js/Parsley/dist/parsley.min.js"></script>

</body>

</html>