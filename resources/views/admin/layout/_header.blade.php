<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    {{-- <link rel="icon" type="image/png" sizes="16x16" href="../assets/plugins/images/favicon.png"> --}}
    <link rel="icon" type="image/png" sizes="16x16" href="{{url('/')}}/uploads/profile_image/{{$site_setting_arr['site_logo']}}">
    <title>{{$page_title or ''}}-{{config('app.project.name')}}</title>
    <!-- Bootstrap Core CSS -->
    <link href="{{url('/')}}/assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{url('/')}}/assets/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css" rel="stylesheet">
    <!-- Menu CSS -->
    <link href="{{url('/')}}/assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css" rel="stylesheet">
    <!-- toast CSS -->
    <link href="{{url('/')}}/assets/plugins/bower_components/toast-master/css/jquery.toast.css" rel="stylesheet">
    <!-- morris CSS -->
    {{-- <link href="{{url('/')}}/assets/plugins/bower_components/morrisjs/morris.css" rel="stylesheet"> --}}
    <!-- animation CSS -->
    <link href="{{url('/')}}/assets/css/animate.css" rel="stylesheet">
    <!-- Custom CSS -->
    <link href="{{url('/')}}/assets/css/style.css" rel="stylesheet">
    <!-- color CSS -->
    <link href="{{url('/')}}/assets/css/colors/blue.css" id="theme" rel="stylesheet">

    <link href="{{url('/')}}/assets/sweetalert/sweetalert.css" rel="stylesheet">

    <link href="{{url('/')}}/assets/js/Parsley/dist/parsley.css" rel="stylesheet">

    <link href="{{url('/')}}/assets/css/custom.css" rel="stylesheet">

    <!-- switechery css-->
    <link href="{{url('/')}}/assets/plugins/bower_components/switchery/dist/switchery.min.css" rel="stylesheet" />

    <!-- Datepicker css-->
    <link href="{{url('/')}}/assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
   
    <script type="text/javascript" src="{{url('/')}}/assets/js/jquery.min.js"></script>

    <!--Datepicker js-->
    <script src="{{url('/')}}/assets/plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
    
    <!-- Dropify -->
    <link rel="stylesheet" href="{{url('/')}}/assets/plugins/bower_components/dropify/dist/css/dropify.min.css">

</head>

<body>
    <!-- Preloader -->
    <div class="preloader">
        <div class="cssload-speeding-wheel"></div>
    </div>
    <div id="wrapper">