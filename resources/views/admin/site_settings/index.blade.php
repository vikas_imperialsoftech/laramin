@extends('admin.layout.master')    

@section('main_content')

   <!-- Page Content -->
  <div id="page-wrapper">
      <div class="container-fluid">
          <div class="row bg-title">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                  <h4 class="page-title">{{$module_title or ''}}</h4> 
                </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> 
                  <ol class="breadcrumb">
                      <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
                      <li class="active">{{$module_title or ''}}</li>
                  </ol>
                </div>
              <!-- /.col-lg-12 -->
          </div>

    <!-- BEGIN Main Content -->
    
    <div class="row">
        <div class="col-sm-12">
            <div class="white-box">
             @include('admin.layout._operation_status')
                <div class="row">
                    <div class="col-sm-12 col-xs-12">
                         <div class="box-title">
                            <h3><i class="ti-settings"></i> {{ isset($page_title)?$page_title:"" }}</h3>
                            <div class="box-tool">
                            </div>
                        </div>
                            
                            {!! Form::open([
                                         'method'=>'POST',   
                                         'class'=>'form-horizontal', 
                                         'id'=>'validation-form',
                                         'enctype' =>'multipart/form-data'
                                        ]) !!}


                                 <div class="input-group">
                                    <label class="col-sm-3 col-lg-2 control-label" for="state">Website Name <i class="red">*</i></label>
                                   
                                        {!! Form::text('site_name',isset($arr_data['site_name'])?$arr_data['site_name']:'',['class'=>'form-control','data-parsley-required'=>'true','data-parsley-maxlength'=>'255']) !!}
                                        <div class="input-group-addon"><i class="ti-world"></i></div>
                                        <span class='red'>{{ $errors->first('site_name') }}</span>
                                   
                                </div>
                                </br>
                                <div class="form-group row">
                                    <input type="hidden" name="old_logo" value="{{isset($arr_data['site_logo'])?$arr_data['site_logo']:''}}">
                                     <label class="col-sm-3 col-lg-2 control-label" for="ad_image">Site Logo<i class="red">*</i> </label>
                                      <div class="col-10">
                                        <input type="file" name="image" id="ad_image" class="dropify" data-default-file="{{url('/')}}/uploads/profile_image/{{isset($arr_data['site_logo'])?$arr_data['site_logo']:''}}"/>
                                      </div>
                                 </div>
                                <br>

                                <div class="input-group">
                                    <label class="col-sm-3 col-lg-2 control-label" for="category_name">Address<i class="red">*</i></label>
                                        {!! Form::text('site_address',isset($arr_data['site_address'])?$arr_data['site_address']:'',['class'=>'form-control','data-parsley-required'=>'true','data-parsley-maxlength'=>'255']) !!}
                                        <span class='red'>{{ $errors->first('site_address') }}</span>
                                </div>
                                <br>
                                <div class="input-group">
                                    <label class="col-sm-3 col-lg-2 control-label">Contact Number<i class="red">*</i></label>
                                        {!! Form::text('site_contact_number',isset($arr_data['site_contact_number'])?$arr_data['site_contact_number']:'',['class'=>'form-control','data-parsley-required'=>'true','data-parsley-maxlength'=>'255', 'data-parsley-digits'=>'true']) !!}
                                        <span class='red'>{{ $errors->first('site_contact_number') }}</span>
                                </div>
                                <br>
                                <div class="input-group">
                                    <label class="col-sm-3 col-lg-2 control-label" for="category_name">Meta Description<i class="red"></i></label>
                                        {!! Form::text('meta_desc',isset($arr_data['meta_desc'])?$arr_data['meta_desc']:'',['class'=>'form-control','data-parsley-required'=>'true','data-parsley-maxlength'=>'255']) !!}
                                        <span class='red'>{{ $errors->first('meta_desc') }}</span>
                                </div>
                                <br>

                                <div class="input-group">
                                    <label class="col-sm-3 col-lg-2 control-label">Meta Keyword</label>
                                    {!! Form::text('meta_keyword',isset($arr_data['meta_keyword'])?$arr_data['meta_keyword']:'',['class'=>'form-control','data-parsley-maxlength'=>'255']) !!}
                                        <span class='red'>{{ $errors->first('meta_keyword') }}</span>
                                </div>
                                <br>

                                <div class="input-group">
                                    <label class="col-sm-3 col-lg-2 control-label">Email<i class="red">*</i></label>
                                        {!! Form::text('site_email_address',isset($arr_data['site_email_address'])?$arr_data['site_email_address']:'',['class'=>'form-control', 'data-parsley-required'=>'true', 'data-parsley-email'=>'true', 'data-parsley-maxlength'=>'255']) !!}
                                    <div class="input-group-addon"><i class="ti-email"></i></div>
                                        <span class='red'>{{ $errors->first('site_email_address') }}</span>
                                </div>
                                <br>

                                <div class="input-group">
                                    <label class="col-sm-3 col-lg-2 control-label">Google Plus URL<i class="red">*</i></label>
                                         {!! Form::text('google_plus_url',isset($arr_data['google_plus_url'])?$arr_data['google_plus_url']:'',['class'=>'form-control','data-parsley-required'=>'true', 'data-parsley-url'=>'true', 'data-parsley-maxlength'=>'500']) !!}
                                         <div class="input-group-addon"><i class="ti-link"></i></div>
                                        <span class='red'>{{ $errors->first('google_plus_url') }}</span>
                                </div>
                                <br>

                                <div class="input-group">
                                    <label class="col-sm-3 col-lg-2 control-label">Facebook URL<i class="red">*</i></label>
                                         {!! Form::text('fb_url',isset($arr_data['fb_url'])?$arr_data['fb_url']:'',['class'=>'form-control','data-parsley-required'=>'true', 'data-parsley-url'=>'true', 'data-parsley-maxlength'=>'500']) !!}
                                         <div class="input-group-addon"><i class="ti-link"></i></div>
                                        <span class='red'>{{ $errors->first('fb_url') }}</span>
                                </div>
                                <br>
                               
                                <div class="input-group">
                                    <label class="col-sm-3 col-lg-2 control-label">Twitter URL<i class="red">*</i></label>
                                        {!! Form::text('twitter_url',isset($arr_data['twitter_url'])?$arr_data['twitter_url']:'',['class'=>'form-control','data-parsley-required'=>'true', 'data-parsley-url'=>'true', 'data-parsley-maxlength'=>'500']) !!}
                                        <div class="input-group-addon"><i class="ti-link"></i></div>
                                        <span class='red'>{{ $errors->first('twitter_url') }}</span>
                                </div>
                                <br>

                                <div class="input-group">
                                    <label class="col-sm-3 col-lg-2 control-label">YouTube URL<i class="red">*</i></label>
                                        {!! Form::text('youtube_url',isset($arr_data['youtube_url'])?$arr_data['youtube_url']:'',['class'=>'form-control','data-parsley-required'=>'true', 'data-parsley-url'=>'true', 'data-parsley-maxlength'=>'500']) !!}
                                        <div class="input-group-addon"><i class="ti-link"></i></div>
                                        <span class='red'>{{ $errors->first('youtube_url') }}</span>
                                </div>
                                <br>

                                <div class="input-group">
                                    <label class="col-sm-3 col-lg-2 control-label">RSS Feed URL </label>
                                        {!! Form::text('rss_feed_url',isset($arr_data['rss_feed_url'])?$arr_data['rss_feed_url']:'',['class'=>'form-control','data-parsley-required'=>'true', 'data-parsley-url'=>'true', 'data-parsley-maxlength'=>'500']) !!}
                                        <div class="input-group-addon"><i class="ti-link"></i></div>
                                        <span class='red'>{{ $errors->first('rss_feed_url') }}</span>
                                </div>    
                                <br>

                                <div class="input-group">
                                    <label class="col-sm-3 col-lg-2 control-label">Instagram URL<i class="red">*</i></label>
                                         {!! Form::text('instagram_url',isset($arr_data['instagram_url'])?$arr_data['instagram_url']:'',['class'=>'form-control','data-parsley-required'=>'true', 'data-parsley-url'=>'true', 'data-parsley-maxlength'=>'500']) !!}
                                         <div class="input-group-addon"><i class="ti-link"></i></div>
                                        <span class='red'>{{ $errors->first('instagram_url') }}</span>
                                </div>  
                                <br>
                                
                                <div class="input-group">
                                    <label class="col-sm-3 col-lg-2 control-label">Site Status<i class="red">*</i></label>&nbsp;&nbsp;&nbsp;
                                        <input type="checkbox" name="site_status" id="site_status" value="1" data-size="small" class="js-switch " @if($arr_data['site_status']=='1') checked="checked" @endif data-color="#99d683" data-secondary-color="#f96262" onchange="return toggle_status();" />
                                </div>  
                                
                                <div class="input-group">
                                    <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                    <input type="hidden" name="enc_id" value="{{base64_encode($arr_data['site_setting_id'])}}">
                                      {{-- <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" value="Update">Update</button>  --}}
                                      <button type="button" class="btn btn-success waves-effect waves-light m-r-10" value="Update" id="btn_update">Update</button>
                                        
                                    </div>
                               </div>
                            {!! Form::close() !!}
                    </div>
                </div>
        </div>
        </div>
    </div>
    
    <!-- END Main Content --> 
    <script type="text/javascript">
      
      $(document).ready(function(){

        $('#btn_update').click(function(){
                  
          // if($('#validation-form').parsley().validate() == false) return;

          var formdata = $('#validation-form').serialize();

          $.ajax({
              url: '{{url('/admin/site_settings/update')}}',
              type:"POST",
              data: new FormData($('#validation-form')[0]),
              contentType:false,
              processData:false,
              dataType:'json',
             success:function(data,status)
             {
              // console.log(status)
              swal(data.status,data.description,data.status);
             }
            });

          });

        });

      function toggle_status()
      {
        var site_status = $('#site_status').val();
        if(site_status=='1')
        {
          $('#site_status').val('1');
        }
        else
        {
          $('#site_status').val('0');
        }
      }

    </script>
@endsection