<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\EnquiryModel;

use Validator;
use Flash;

class FrontController extends Controller
{
	public function __construct(EnquiryModel $enquiry_model)
	{
		$this->module_view_folder = "front";
		$this->EnquiryModel = $enquiry_model;
		// $this->module_url_path    = url('/')."store");
 		$this->arr_view_data      = [];
		# code...
	}
    //
    public function index()
    {
    	// $this->arr_view_data['module_url_path'] = $this->module_url_path;
    	return view($this->module_view_folder.'/index');
    }

    public function store(Request $request)
    {
    	$frm_data = $request->all();
    	$arr_rules = [
            'first_name' 	=> 'required',
            'last_name'   	=> 'required',
            'email'   		=> 'required',
            'cmb_country'   => 'required'
        ];

    	$validator = Validator::make($request->all(),$arr_rules,[
                'first_name.required' 		=> 'First name is required',
                'last_name.required' 		=> 'Last name is required',
                'email.required' 			=> 'Email is required',
                'cmb_country.required' 		=> 'Country is required'
            ]);

        if($validator->fails())
        {
            $response['status'] = 'warning';
            $response['description'] = 'Valiation Failed Please check The All Fields In Form.';

            return redirect()->back();
        }

        // dd($frm_data['radiog_dark']);
        if(isset($frm_data['radiog_dark']) && count($frm_data['radiog_dark'])>0)
        {
        	$radiog_dark = implode(',', $frm_data['radiog_dark']);
		}

		if(isset($frm_data['receive_detail']) && !empty($frm_data['receive_detail']))
		{
			$receive_detail = $frm_data['receive_detail'];
		}
		else
		{
			$receive_detail = "YES";	
		}

        $insert_arr = array(
        					'first_name'	=> $frm_data['first_name'],
        					'last_name'		=> $frm_data['last_name'],
        					'email'			=> $frm_data['email'],
        					'cmb_country'	=> $frm_data['cmb_country'],
        					'receive_detail'=> $receive_detail,
        					'radiog_dark'	=> $radiog_dark
    						);
        
        $insert = $this->EnquiryModel->create($insert_arr);
        if($insert)
        {
        	Flash('success','Enquery send successfully');
        	return redirect()->back();
        }
        else
        {
        	Flash('error','Unable to Send Enquery');
        }
    }
}
