<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Common\Traits\MultiActionTrait;

use App\Models\UserModel;
use App\Models\CitiesModel;
use App\Models\statesModel;
use App\Models\CountriesModel;
use App\Models\CountryModel;
use App\Events\ActivityLogEvent;
use App\Models\ActivityLogsModel;
use Flash;
use Validator;
use Sentinel;
use Activation;

use DB;
use Datatables;

class UserController extends Controller
{
    use MultiActionTrait;

    public function __construct(UserModel $user,
                                CountryModel $country,
                                ActivityLogsModel $activity_logs,
                                statesModel $statesModel,
                                CitiesModel $CitiesModel,
                                CountriesModel $CountriesModel)
    {
        $user = Sentinel::createModel();

        $this->UserModel                    = $user;
        $this->BaseModel                    = Sentinel::createModel();   // using sentinel for base model.
        $this->CountryModel                 = $country;
        $this->statesModel                  = $statesModel;
        $this->CitiesModel                  = $CitiesModel;
        $this->CountriesModel               = $CountriesModel;
        $this->ActivityLogsModel            = $activity_logs;

        $this->user_profile_base_img_path   = base_path().config('app.project.img_path.user_profile_image');
        $this->user_profile_public_img_path = url('/').config('app.project.img_path.user_profile_image');

        $this->arr_view_data                = [];
        $this->module_url_path              = url(config('app.project.admin_panel_slug')."/users");
        $this->module_title                 = "Users";
        $this->modyle_url_slug              = "users";
        $this->module_view_folder           = "admin.users";
       
    }	

    public function index()
    {
        $this->arr_view_data['arr_data'] = array();
        $obj_data = $this->BaseModel->whereHas('roles',function($query)
                                        {
                                            $query->where('slug','!=','admin');        
                                        })
        							->get();
        if($obj_data)
        {
        	$arr_data = $obj_data->toArray();
        }	

        $this->arr_view_data['page_title']      = "Manage ".str_plural($this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['arr_data']        = $arr_data;
        

        return view($this->module_view_folder.'.index', $this->arr_view_data);
    }

    function get_users_details(Request $request)
    {
        $user_details           = $this->BaseModel->getTable();
        
        $prefixed_user_details  = DB::getTablePrefix().$this->BaseModel->getTable();


        $obj_user = DB::table($user_details)
                                ->select(DB::raw($prefixed_user_details.".id as id,".
                                                 $prefixed_user_details.".email as email, ".
                                                 $prefixed_user_details.".is_active as is_active, ".
                                                 "CONCAT(".$prefixed_user_details.".first_name,' ',"
                                                          .$prefixed_user_details.".last_name) as user_name"
                                                 ))
                                ->whereNull($user_details.'.deleted_at')
                                ->where('id','!=',1)
                                ->orderBy($user_details.'.created_at','DESC');

        /* ---------------- Filtering Logic ----------------------------------*/                    
        $arr_search_column = $request->input('column_filter');
        
        if(isset($arr_search_column['q_name']) && $arr_search_column['q_name']!="")
        {
            $search_term      = $arr_search_column['q_name'];
            $obj_user = $obj_user->having('user_name','LIKE', '%'.$search_term.'%');
        }

        if(isset($arr_search_column['q_email']) && $arr_search_column['q_email']!="")
        {
            $search_term      = $arr_search_column['q_email'];
            $obj_user = $obj_user->where($user_details.'.email','LIKE', '%'.$search_term.'%');
        }

        return $obj_user;
    }

    public function get_records(Request $request)
    {
        $obj_user     = $this->get_users_details($request);

        $current_context = $this;

        $json_result     = Datatables::of($obj_user);

        $json_result     = $json_result->blacklist(['id']);
        
        $json_result     = $json_result->editColumn('enc_id',function($data) use ($current_context)
                            {
                                return base64_encode($data->id);
                            })
                            ->editColumn('build_status_btn',function($data) use ($current_context)
                            {
                                $build_status_btn ='';
                                if($data->is_active != null && $data->is_active == '0')
                                {   
                                    $build_status_btn = '<input type="checkbox" data-size="small" data-enc_id="'.base64_encode($data->id).'"  class="js-switch toggleSwitch" data-type="activate" data-color="#99d683" data-secondary-color="#f96262" />';
                                }
                                elseif($data->is_active != null && $data->is_active == '1')
                                {
                                    $build_status_btn = '<input type="checkbox" checked data-size="small"  data-enc_id="'.base64_encode($data->id).'"  id="status_'.$data->id.'" class="js-switch toggleSwitch" data-type="deactivate" data-color="#99d683" data-secondary-color="#f96262" />';
                                }
                                return $build_status_btn;
                            })    
                            ->editColumn('build_action_btn',function($data) use ($current_context)
                            {   
                                $edit_href =  $this->module_url_path.'/edit/'.base64_encode($data->id);
                                $build_edit_action = '<a class="btn btn-outline btn-info btn-circle show-tooltip" href="'.$edit_href.'" title="Edit"><i class="ti-pencil-alt2" ></i></a>';

                                $delete_href =  $this->module_url_path.'/delete/'.base64_encode($data->id);

                                $confirm_delete = 'onclick="confirm_delete(this,event);"';
                                
                                $build_delete_action = '<a class="btn btn-circle btn-danger btn-outline show-tooltip" '.$confirm_delete.' href="'.$delete_href.'" title="Delete"><i class="ti-trash" ></i></a>';
                                return $build_action = $build_edit_action.' '.$build_delete_action;
                            })
                            ->make(true);

        $build_result = $json_result->getData();
        
        return response()->json($build_result);
    }

    public function create()
    {
        $obj_country = $this->CountryModel->where('is_active','=','1')->get(['id','country_code','country_name']);  
        if($obj_country)
        {
            $arr_country = $obj_country->toArray();                    
        }

        $this->arr_view_data['countries']       = $this->CountriesModel->get()->toArray();
        $this->arr_view_data['page_title']      = "Create ".str_singular($this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        $this->arr_view_data['arr_country']     = $arr_country;
        
        return view($this->module_view_folder.'.create', $this->arr_view_data);
    }

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);
        
        $obj_user = $this->BaseModel->where('id','=',$id)
                                    ->first(['id','email','first_name','last_name','profile_image','country','state','city','street_address','zipcode','phone']);
        $arr_data = $arr_country = [];                                    
        
        if($obj_user)
        {
            $arr_data = $obj_user->toArray();
        }  

        $obj_country = $this->CountryModel->where('is_active','=','1')->get(['id','country_code','country_name']);  
        
        if($obj_country)
        {
            $arr_country = $obj_country->toArray();   
                ;
        }                                     
        
        $this->arr_view_data['countries']                    = $this->CountriesModel->get();
        $this->arr_view_data['states']                       = $this->statesModel->get();
        $this->arr_view_data['page_title']                   = "Edit ".str_singular($this->module_title);
        $this->arr_view_data['module_title']                 = str_plural($this->module_title);
        $this->arr_view_data['module_url_path']              = $this->module_url_path;
        $this->arr_view_data['arr_data']                     = $arr_data;
        $this->arr_view_data['arr_country']                  = $arr_country;
        $this->arr_view_data['user_profile_public_img_path'] = $this->user_profile_public_img_path;
        
        return view($this->module_view_folder.'.edit', $this->arr_view_data);
    }

    public function get_states($country_id)
    {
     $citys = $this->statesModel->where('country_id',$country_id)->get()->toArray();
     return response()->json($citys);
    }

    public function save(Request $request)
    {    
        $is_update = false;

        $formdata  = $request->all();
                
        $user_id   = $request->input('user_id',false);

        $user_id   = $user_id == "" ? false : $user_id;

        if($request->has('user_id'))
        {
           $is_update = true;
        }       

        /*Check validations*/
        $arr_rules = [
                        'first_name'    => 'required',
                        'last_name'     => 'required',
                        'email'         => 'required|email',
                        'country'       => 'required',
                        'street_address'=> 'required',
                        'phone'         => 'required|numeric',
                        'state'         => 'required',
                     ];

        if($is_update == false)
        {
            $arr_rules['profile_image'] = 'required';
            $arr_rules['password']      = 'required';
        }

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            $response['status']      = 'warning';
            $response['description'] = 'Valiation Failed Please check The All Fields In Form.';
             return response()->json($response);
         }

        /* Check for email duplication */
        $is_duplicate = $this->BaseModel->where('email',$formdata['email']);  
        
        if($is_update)
        {
            $is_duplicate = $is_duplicate->where('id','<>',$user_id);
        }

        $does_exists = $is_duplicate->count();

        if($does_exists)
        {
            $response['status']      = 'warning';
            $response['description'] = 'Alredy Exists.';

            return response()->json($response);
        }   
         
        $user =  Sentinel::createModel()->firstOrNew(['id' => $user_id]);

        $user->first_name = $formdata['first_name'];
        $user->email      = $formdata['email'];
        
        if($formdata['password'])
        {
           $hasher           = Sentinel::getHasher();
           $user->password   = $hasher->hash($formdata['password']);
        }
       
        $user_details     = $user->save();
        
        if($is_update == false)
        {
            /* Activate User By Default */
            $activation = Activation::create($user);    

            if($activation)
            {
                Activation::complete($user,$activation->code);
            }
        }

        $profile_image = $request->file('profile_image');
        $file_name      = "default.jpg";

        if(isset($profile_image))
        {
            /* User Proof upload */
            $file_extension = strtolower($request->file('profile_image')->getClientOriginalExtension()); 
            
            $file_name      = sha1(uniqid().$file_name.uniqid()).'.'.$file_extension;

            if($request->file('profile_image')->move($this->user_profile_base_img_path, $file_name)) 
            {
                if(isset($profile_image))
                {
                    $user->profile_image  = $file_name;    
                }

                // /* Unlink the Existing file from the folder */
                $obj_image = $this->BaseModel->where('id',$user_id)->first(['profile_image']);
              
                if($obj_image)   
                {   
                    $_arr = [];
                    $_arr = $obj_image->toArray();

                    if(isset($_arr['profile_image']) && $_arr['profile_image'] != "" )
                    {
                      $unlink_path    = $this->user_profile_base_img_path.$_arr['profile_image'];
                      @unlink($unlink_path);
                    }
                }    
            }
        }

        if($user_details)
        {   
            $role = Sentinel::findRoleById($user->id);
            
            if($role != false && $user->inRole($role->slug) == false)
            {
               $role->users()->attach($user);    
            }
          
            $user->last_name      = $formdata['last_name'];
            $user->country        = $formdata['country'];
            $user->state          = $formdata['state'];
            // $user->city           = $formdata['city'];
            $user->street_address = $formdata['street_address'];
            $user->zipcode        = $formdata['zipcode'];
            $user->phone          = $formdata['phone'];
            $user->is_active      = '1';
            $user->save();

            /*-------------------------------------------------------
            |   Activity log Event
            --------------------------------------------------------*/
            $arr_event['ACTION']       = 'ADD';

            if($is_update)
            {
                $arr_event['ACTION']   = 'EDIT';
            }

            $arr_event['MODULE_TITLE'] = $this->module_title;

            $this->save_activity($arr_event);

            /*----------------------------------------------------------------------*/

            $response['status'] = 'success';
            $response['description'] = str_singular($this->module_title).' Save Successfully';

            if($is_update == false)
            {
                if($user->id)
                {
                    $response['link'] = url('/admin/users/edit/'.base64_encode($user->id));
                }
            }

        }
        else
        {
            $response['status'] = 'error';
            $response['description'] = 'Problem Occured While Creating '.str_singular($this->module_title);
        }   

        return response()->json($response);
    }

    public function activate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }

        if($this->perform_activate(base64_decode($enc_id)))
        {
             $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'ACTIVE';
        return response()->json($arr_response);
    }

    public function deactivate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
            $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'DEACTIVE';

        return response()->json($arr_response);
    }

    public function delete($enc_id = FALSE)
    {

        if(!$enc_id)
        {
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {   
            Flash::success(str_singular($this->module_title).' Deleted Successfully');
        }
        else
        {
            Flash::error('Problem Occured While '.str_singular($this->module_title).' Deletion ');
        }

        return redirect()->back();
    }

     /*
    | multi_action() : mutiple actions like active/deactive/delete for multiple slected records
    | auther : Paras Kale 
    | Date : 01-02-2016    
    | @param  \Illuminate\Http\Request  $request
    */
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            Flash::error('Please Select '.str_plural($this->module_title) .' To Perform Multi Actions');  
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem Occured, While Doing Multi Action');
            return redirect()->back();

        }
        
        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Flash::success(str_plural($this->module_title).' Deleted Successfully'); 
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Flash::success(str_plural($this->module_title).' Activated Successfully'); 
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Flash::success(str_plural($this->module_title).' Blocked Successfully');  
            }
        }

        return redirect()->back();
    }

    public function perform_activate($id)
    {
        $entity = $this->BaseModel->where('id',$id)->first();
        

        if($entity)
        {
            return $this->BaseModel->where('id',$id)->update(['is_active'=>'1']);
        }

        return FALSE;
    }

    public function perform_deactivate($id)
    {

        $entity = $this->BaseModel->where('id',$id)->first();
        
        if($entity)
        {
            return $this->BaseModel->where('id',$id)->update(['is_active'=>'0']);
        }
        return FALSE;
    }

    public function perform_delete($id)
    {
        $entity = $this->BaseModel->where('id',$id)->first();
        if($entity)
        {

            /* Detaching Role from user Roles table */
            $user = Sentinel::findById($id);
            $role_owner     = Sentinel::findRoleBySlug('owner');
            $role_traveller = Sentinel::findRoleBySlug('traveller');
            $user->roles()->detach($role_owner);
            $user->roles()->detach($role_traveller);

           $delete_success = $this->BaseModel->where('id',$id)->delete();
            /*-------------------------------------------------------
            |   Activity log Event
            --------------------------------------------------------*/
                $arr_event                 = [];
                $arr_event['ACTION']       = 'REMOVED';
                $arr_event['MODULE_TITLE'] = $this->module_title;

                $this->save_activity($arr_event);
            /*----------------------------------------------------------------------*/
           return $delete_success;
        }

        return FALSE;
    }

    public function build_select_options_array(array $arr_data,$option_key,$option_value,array $arr_default)
    {

        $arr_options = [];
        if(sizeof($arr_default)>0)
        {
            $arr_options =  $arr_default;   
        }

        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data) 
            {
                if(isset($data[$option_key]) && isset($data[$option_value]))
                {
                    $arr_options[$data[$option_key]] = $data[$option_value];
                }
            }
        }

        return $arr_options;
    }
}
