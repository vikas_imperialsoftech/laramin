<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\SiteSettingModel;
use App\Events\ActivityLogEvent;
use App\Models\ActivityLogsModel;

use Validator;
use Flash;
use Input;
use Sentinel;
 
class SiteSettingController extends Controller
{
    
    public function __construct(SiteSettingModel $siteSetting,
                                ActivityLogsModel $activity_logs)
    {
        $this->SiteSettingModel   = $siteSetting;
        $this->arr_view_data      = [];
        $this->BaseModel          = $this->SiteSettingModel;
        $this->ActivityLogsModel  = $activity_logs;

        $this->user_base_img_path   = base_path().config('app.project.img_path.user_profile_image');
        $this->user_public_img_path = url('/').config('app.project.img_path.user_profile_image');

        $this->module_title       = "Site Settings";
        $this->module_view_folder = "admin.site_settings";
        $this->module_url_path    = url(config('app.project.admin_panel_slug')."/site_settings");
    }

    public function index()
    {
        $arr_data = array();   

        $obj_data =  $this->BaseModel->first();

        if($obj_data != FALSE)
        {
            $arr_data = $obj_data->toArray();    
        }

        $this->arr_view_data['arr_data']        = $arr_data;
        $this->arr_view_data['page_title']      = str_singular($this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        
        return view($this->module_view_folder.'.index',$this->arr_view_data);
    }

    public function update(Request $request)
    {
        $formdata = $request->all();
        
        $id = base64_decode($formdata['enc_id']);

        $arr_rules = [
                        'site_name'=>'required',
                        'site_email_address'=>'required|email',
                        'site_contact_number'=>'required|numeric',
                        'site_address'=>'required',
                        'fb_url'=>'required',
                        'google_plus_url'=>'required',
                        'twitter_url'=>'required',
                        'youtube_url'=>'required',
                        'instagram_url'=>'required',
                        'meta_desc'=>'required',
                        'meta_keyword'=>'required',
                        'rss_feed_url'=>'required'
                    ];

        $validate = Validator::make($request->all(),$arr_rules);

        if($validate->fails())
        {
            $response['status'] = 'warning';
            $response['description'] = 'Form Validation Failed Please Check Form Fields.';

            return response()->json($response);
        }

        $file_name = "default.jpg";
        
        if ($request->hasFile('image')) 
        {
            $file_name = $request->input('image');
            $file_extension = strtolower($request->file('image')->getClientOriginalExtension()); 

            $file_name = sha1(uniqid().$file_name.uniqid()).'.'.$file_extension;
            $request->file('image')->move($this->user_base_img_path, $file_name);  
        }
        else
        {
            $file_name = $request->input('old_logo');
        }

        if($formdata['site_status']=='1')
        {
            $site_status = $formdata['site_status'];
        }
        else
        {
            $site_status = '0';
        }

        $arr_data['site_name']           = $formdata['site_name'];
        $arr_data['site_address']        = $formdata['site_address'];
        $arr_data['site_contact_number'] = $formdata['site_contact_number'];
        $arr_data['meta_desc']           = $formdata['meta_desc'];
        $arr_data['meta_keyword']        = $formdata['meta_keyword'];
        $arr_data['site_email_address']  = $formdata['site_email_address'];
        $arr_data['site_logo']           = $file_name;
        $arr_data['fb_url']              = $formdata['fb_url'];
        $arr_data['google_plus_url']     = $formdata['google_plus_url'];
        $arr_data['twitter_url']         = $formdata['twitter_url'];
        $arr_data['youtube_url']         = $formdata['youtube_url'];
        $arr_data['rss_feed_url']        = $formdata['rss_feed_url'];
        $arr_data['instagram_url']       = $formdata['instagram_url'];
        $arr_data['site_status']         = $site_status;
        
        $entity = $this->BaseModel->where('site_setting_id',$id)->update($arr_data);

        if($entity)
        {
            /*-------------------------------------------------------
            |   Activity log Event
            --------------------------------------------------------*/
            $arr_event                 = [];
            $arr_event['ACTION']       = 'EDIT';
            $arr_event['MODULE_TITLE'] = $this->module_title;

            $this->save_activity($arr_event);
            /*----------------------------------------------------------------------*/
            
            $response['status'] = 'success';
            $response['description'] = str_singular($this->module_title).' Save Successfully.';
        }
        else
        {
           $response['status'] = 'success';
           $response['description'] = str_singular($this->module_title).' Save Successfully.';
        } 
           return response()->json($response);
    }
}
