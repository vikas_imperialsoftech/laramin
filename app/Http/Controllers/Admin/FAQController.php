<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Common\Traits\MultiActionTrait;

use App\Models\FAQModel;  
use App\Common\Services\LanguageService;
use App\Events\ActivityLogEvent;
use App\Models\ActivityLogsModel;
use App\Models\LanguageModel;

use Sentinel;
use Validator;
use Flash;
 
class FAQController extends Controller
{
    use MultiActionTrait;

    public function __construct(FAQModel $faq,
                                LanguageService $langauge,
                                ActivityLogsModel $activity_logs,
                                LanguageModel $langauges)
    {        
        $this->arr_view_data     =   [];
        
        $this->LanguageModel     =   $langauges;
        $this->FAQModel          =   $faq;
        $this->BaseModel         =   $this->FAQModel;
        $this->ActivityLogsModel = $activity_logs;
        
        $this->module_title      =   "FAQ's";
        $this->LanguageService   =   $langauge;
        $this->module_url_path   =   url(config('app.project.admin_panel_slug')."/faq");
        $this->module_view_folder=   "admin.faq";
    }   
 
    public function index()
    {
        $arr_lang = array();
        $arr_data = array();
        $obj_data = $this->BaseModel->orderBy('id','DESC')->get();
        $arr_lang = $this->LanguageService->get_all_language();

        if(sizeof($obj_data)>0)
        {
            foreach ($obj_data as $key => $data) 
            {
                $arr_tmp = array();
                // Check Language Wise Transalation Exists
                foreach ($arr_lang as $key => $lang) 
                {
                    $arr_tmp[$key]['title']     = $lang['title'];
                    $arr_tmp[$key]['is_avail']  = $data->hasTranslation($lang['locale']);
                }    
                    $data->arr_translation_status = $arr_tmp;
        // Call to hasTranslation method of object is triggering translations so need to unset it 
                    unset($obj_data->translations);
            }
        }

        if($obj_data != FALSE)
        {
            $arr_data = $obj_data->toArray();
        }
        
        $this->arr_view_data['arr_data']        = $arr_data;
        $this->arr_view_data['page_title']      = "Manage ".str_singular( $this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view($this->module_view_folder.'.index',$this->arr_view_data);
    }

    public function create()
    {
        $this->arr_view_data['arr_lang']        = $this->LanguageService->get_all_language();  
        $this->arr_view_data['page_title']      = "Create ".$this->module_title;
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view($this->module_view_folder.'.create',$this->arr_view_data);
    }

    public function edit($enc_id)
    {
        $id       = base64_decode($enc_id);
        $this->arr_view_data['count'] = $this->LanguageModel->where('status','1')->count();
        $arr_lang = $this->LanguageService->get_all_language();
        
        $obj_data = $this->BaseModel->where('id', $id)->with(['translations'])->first();
        $arr_data = [];

        if($obj_data)
        {
           $arr_data = $obj_data->toArray();
           /* Arrange Locale Wise */
           $arr_data['translations'] = $this->arrange_locale_wise($arr_data['translations']);
        }

        $this->arr_view_data['edit_mode']       = TRUE;
        $this->arr_view_data['enc_id']          = $enc_id;
        $this->arr_view_data['arr_lang']        = $this->LanguageService->get_all_language();  
        $this->arr_view_data['arr_data']        = $arr_data; 
        $this->arr_view_data['page_title']      = "Edit ".$this->module_title;
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view($this->module_view_folder.'.edit',$this->arr_view_data);
    }

    public function save(Request $request)
    {
        $is_update = false;

        $form_data = $request->all();

        $enc_id = base64_decode($request->input('enc_id',false));
        $enc_id = $enc_id == "" ? false : $enc_id;

        if($request->has('enc_id'))
        {
            $is_update = true;
        }

        /*Check Validations*/
        $arr_rules = [
                        'question_en' => 'required',
                        'answer_en'   => 'required'
                    ];

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
          $response['status'] = 'warning';
          $response['description'] = 'Form Validation Failed Please Check Form Fields.';

          return response()->json($response);
        }

        /* Check if location already exists with given translation */
        $is_duplicate = $this->BaseModel
                            ->whereHas('translations',function($query) use($request)
                                        {
                                            $query->where('locale','en')
                                                  ->where('question',$request->input('question_en'));
                                        });

        if($is_update)
        {
           $is_duplicate= $is_duplicate->where('id','<>',$enc_id);
        }
          
        $does_exists = $is_duplicate->count();
       
        if($does_exists)
        {
           $response['status']      = 'warning';
           $response['description'] = str_singular($this->module_title).' Already Exists.';

           return response()->json($response);
        }

        $entity = FAQModel::firstOrNew(['id' => $enc_id]);

        /* Insert into Location Table */

        $entity->is_active = '1';

        $entity_details = $entity->save();

        if($entity_details)      
        {
            $arr_lang =  $this->LanguageService->get_all_language();      
         
            /* insert record into translation table */
            if(sizeof($arr_lang) > 0 )
            {
                foreach ($arr_lang as $lang) 
                {            
                    $arr_data = array();
                    $question = $form_data['question_'.$lang['locale']];
                    $answer   = $form_data['answer_'.$lang['locale']];

                    if( (isset($question) && $question != '') && (isset($answer) && $answer != '') )
                    { 
                      
                        /* Get Existing Language Entry */
                        $translation = $entity->getTranslation($lang['locale']);  

                        if($translation)
                        {
                            $translation->question  = $question;
                            $translation->answer    = $answer;
                            $translation->save();    
                        }  
                        else
                        {
                            /* Create New Language Entry  */
                            $translation = $entity->getNewTranslation($lang['locale']);
                            $translation->faq_id    = $entity->id;
                            $translation->question  = $question;
                            $translation->answer    = $answer;
                            $translation->save();
                        }                       
                    }
                }//foreach 
                /*-------------------------------------------------------
                   |   Activity log Event
                --------------------------------------------------------*/
                $arr_event                 = [];
                $arr_event['ACTION']       = 'ADD';

                if($is_update)
                {
                    $arr_event['ACTION']   = 'EDIT';
                }

                $arr_event['MODULE_TITLE'] = $this->module_title;

                $this->save_activity($arr_event);

               /*----------------------------------------------------------------------*/
                      
                if($is_update == false)
                {
                    if($entity->id)
                    {
                        $response['link'] = url('/admin/faq/edit/'.base64_encode($entity->id));
                    }
                }
            } //if
               $response['status'] = 'success';
               $response['description'] = str_singular($this->module_title).' Save Successfully.';          
        }
         else
         {
            $response['status'] = 'error';
            $response['description'] = 'Error Occured While Save '.str_singular($this->module_title).'.';
         }

        return response()->json($response);
        // if($entity_details)
        // {
        //     $enc_id = base64_encode($entity->id);
        //     return redirect($this->module_url_path.'/edit/'.$enc_id);
        // }
        // else
        // {    
        //     return redirect()->back();
        // }
    }


    public function activate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }

        $arr_response = [];    
        if($this->perform_activate(base64_decode($enc_id)))
        {
            $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'ACTIVE';

        return response()->json($arr_response);
    }

    public function deactivate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }
        $arr_response = []; 

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
             $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'DEACTIVE';

        return response()->json($arr_response);
    }

    public function perform_activate($id)
    {
        $category = $this->BaseModel->where('id',$id)->update(['is_active'=>'1']);
    
        if($category)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function perform_deactivate($id)
    {
        $category     = $this->BaseModel->where('id',$id)->update(['is_active'=>'0']);

        if($category)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    public function arrange_locale_wise(array $arr_data)
    {
        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data) 
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);

                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }
    
    // public function multi_action(Request $request)
    // {
    //     $arr_rules = array();
    //     $arr_rules['multi_action'] = "required";
    //     $arr_rules['checked_record'] = "required";


    //     $validator = Validator::make($request->all(),$arr_rules);

    //     if($validator->fails())
    //     {
    //         return redirect()->back()->withErrors($validator)->withInput();
    //     }

    //     $multi_action = $request->input('multi_action');
    //     $checked_record = $request->input('checked_record');

    //     /* Check if array is supplied*/
    //     if(is_array($checked_record) && sizeof($checked_record)<=0)
    //     {
    //         Session::flash('error','Problem Occured, While Doing Multi Action');
    //         return redirect()->back();

    //     }

        
    //     foreach ($checked_record as $key => $record_id) 
    //     {  
    //         if($multi_action=="delete")
    //         {
    //            $this->perform_delete(base64_decode($record_id));    
    //            Flash::success(str_plural($this->module_title).' Deleted Successfully'); 
    //         } 
    //         elseif($multi_action=="activate")
    //         {
    //            $this->perform_activate(base64_decode($record_id)); 
    //            Flash::success(str_plural($this->module_title).' Activated Successfully'); 
    //         }
    //         elseif($multi_action=="deactivate")
    //         {
    //            $this->perform_deactivate(base64_decode($record_id));    
    //            Flash::success(str_plural($this->module_title).' Blocked Successfully');  
    //         }
    //     }

    //     return redirect()->back();
    // }

}
