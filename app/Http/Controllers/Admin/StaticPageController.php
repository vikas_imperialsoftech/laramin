<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Common\Traits\MultiActionTrait;

use App\Common\Services\LanguageService;  
use App\Models\StaticPageModel;
use App\Events\ActivityLogEvent;
use App\Models\ActivityLogsModel;   

use Validator;
use Flash;
Use Sentinel;
 
class StaticPageController extends Controller
{
    use MultiActionTrait;
    
    public $StaticPageModel; 
    
    public function __construct(StaticPageModel $static_page,
                                LanguageService $langauge,
                                ActivityLogsModel $activity_logs)
    {      
        $this->StaticPageModel   = $static_page;
        $this->BaseModel         = $this->StaticPageModel;
        $this->ActivityLogsModel = $activity_logs;

        $this->LanguageService   = $langauge;
        $this->module_title      = "CMS";
        $this->module_url_slug   = "static_pages";
        $this->module_url_path   = url(config('app.project.admin_panel_slug')."/static_pages");
    }
   
    public function index()
    {
        $arr_lang   =  $this->LanguageService->get_all_language();  

        $obj_static_page = $this->BaseModel->orderBy('id','DESC')->get();

        if($obj_static_page != FALSE)
        {
            $arr_static_page = $obj_static_page->toArray();
        }

        $this->arr_view_data['arr_static_page'] = $arr_static_page; 
        $this->arr_view_data['page_title']      = "Manage ".$this->module_title;
        $this->arr_view_data['module_title']    = $this->module_title;
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

       return view('admin.static_page.index',$this->arr_view_data);
    }

    public function create()
    {
        $this->arr_view_data['arr_lang']        = $this->LanguageService->get_all_language();
        $this->arr_view_data['page_title']     = "Create ".$this->module_title;
        $this->arr_view_data['module_title']    = $this->module_title;
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
       
        return view('admin.static_page.create',$this->arr_view_data);
    }

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);
        
        $arr_lang = $this->LanguageService->get_all_language();      

        $obj_static_page = $this->BaseModel->where('id', $id)->with(['translations'])->first();

        $arr_static_page = [];

        if($obj_static_page)
        {
           $arr_static_page = $obj_static_page->toArray(); 

           /* Arrange Locale Wise */
           $arr_static_page['translations'] = $this->arrange_locale_wise($arr_static_page['translations']);
        }

        $this->arr_view_data['edit_mode'] = TRUE;
        $this->arr_view_data['enc_id']    = $enc_id;
        $this->arr_view_data['arr_lang']  = $this->LanguageService->get_all_language();
        
        $this->arr_view_data['arr_static_page'] = $arr_static_page;
        $this->arr_view_data['page_title']      = "Edit ".$this->module_title;
        $this->arr_view_data['module_title']    = $this->module_title;
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
       
        return view('admin.static_page.edit',$this->arr_view_data);  

    }

    public function save(Request $request)
    {
        $is_update = false;

        $form_data = $request->all();

        $enc_id = base64_decode($request->input('enc_id',false));

        $enc_id = $enc_id == "" ? false : $enc_id;

        if($request->has('enc_id'))
        {
          $is_update = true;
        }

        $arr_rules = [
                        'page_title_en'  =>'required',
                        'meta_keyword_en'=>'required',
                        'meta_desc_en'   =>'required',
                        'page_desc_en'   =>'required'
                     ];

        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
          $response['status']      = 'warning';
          $response['description'] = 'Form Validation Failed Please Check Form Fields.';

          return response()->json($response);
        }
        
        $page = $this->StaticPageModel->firstOrNew(['id' => $enc_id]);

        $page->page_slug = str_slug($form_data['page_title_en']);
        $page->is_active = 1;            
              
        $page_details    = $page->save();

        $static_page_id  = $page->id;

        /* Fetch All Languages*/
        $arr_lang =  $this->LanguageService->get_all_language();

        if(sizeof($arr_lang) > 0 )
        {
            foreach ($arr_lang as $lang) 
            {            
                $arr_data     = array();
                $page_title   = 'page_title_'.$lang['locale'];
                $meta_keyword = 'meta_keyword_'.$lang['locale'];
                $meta_desc    = 'meta_desc_'.$lang['locale'];
                $page_desc    = 'page_desc_'.$lang['locale'];

                if( isset($form_data[$page_title]) && $form_data[$page_title] != '')
                { 
                    $translation = $page->getTranslation($lang['locale']);    

                    if($translation)
                    {
                        $translation->page_title    =  $form_data['page_title_'.$lang['locale']];
                        $translation->meta_keyword  =  $form_data['meta_keyword_'.$lang['locale']];
                        $translation->meta_desc     =  $form_data['meta_desc_'.$lang['locale']];
                        $translation->page_desc     =  $form_data['page_desc_'.$lang['locale']];

                        $translation->save();    
                    }  
                    else
                    {
                        $translation = $page->translateOrNew($lang['locale']);

                        $translation->page_title      = $form_data[$page_title];
                        $translation->meta_keyword    = $form_data[$meta_keyword];
                        $translation->meta_desc       = $form_data[$meta_desc];
                        $translation->page_desc       = $form_data[$page_desc];
                        $translation->static_page_id  = $static_page_id;

                        $translation->save();
                    }        
                }

            }//foreach
             /*-------------------------------------------------------
              |   Activity log Event
              --------------------------------------------------------*/
             $arr_event                 = [];
             $arr_event['ACTION']       = 'ADD';

             if($is_update)
             {
               $arr_event['ACTION']    = 'EDIT';
             }
             $arr_event['MODULE_TITLE'] = $this->module_title;

             $this->save_activity($arr_event);

            /*----------------------------------------------------------------------*/
                   
             $response['status']      = 'success';
             $response['description'] = $this->module_title .' Save Successfully.';

             if($is_update == false)
             {
                if($static_page_id)
                {
                    $response['link'] = url('/admin/static_pages/edit/'.base64_encode($static_page_id));
                }
             }

        } //if
        else
        {
            $response['status']      = 'error';
            $response['description'] = 'Error Occured While Save '.$this->module_title.'.' ;            
        }
        return response()->json($response);
        // return redirect()->back();
    }

    public function activate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }

        $arr_response = [];    
        if($this->perform_activate(base64_decode($enc_id)))
        {
            $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'ACTIVE';

        return response()->json($arr_response);
    }

    public function deactivate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }
        $arr_response = []; 
        if($this->perform_deactivate(base64_decode($enc_id)))
        {
             $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'DEACTIVE';
    }
     
    public function perform_activate($id)
    {
        $cms = $this->BaseModel->where('id',$id)->update(['is_active'=>'1']);
        if($cms)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function perform_deactivate($id)
    {
        $cms     = $this->BaseModel->where('id',$id)->update(['is_active'=>'0']);
        
        if($cms)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    public function arrange_locale_wise(array $arr_data)
    {
        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data) 
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);

                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }


}