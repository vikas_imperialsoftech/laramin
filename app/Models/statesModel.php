<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class statesModel extends Model
{
   use SoftDeletes;
	
    protected $table = 'state';

    protected $fillable = ['name','country_id'];

	
	public function get_country()
	{
	  return $this->belongsTo('App\Models\CountriesModel','country_id','id') ;
	}
	
}
