<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class CountriesModel extends Model
{
    use SoftDeletes;
    protected $table = 'countries';

    protected $fillable = ['country_code','country_name','phonecode','is_active'];
}
