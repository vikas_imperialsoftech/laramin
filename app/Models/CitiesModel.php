<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CitiesModel extends Model
{
    use SoftDeletes;

    protected $table = 'cities';

    protected $fillable = ['name','state_id'];

    public function get_state()
    {
    	return $this->belongsTo('App\Models\statesModel','state_id','id');
    }
}
