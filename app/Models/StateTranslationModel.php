<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Watson\Rememberable\Rememberable;
use Illuminate\Database\Eloquent\SoftDeletes;

class StateTranslationModel extends Model
{
	use Rememberable,SoftDeletes;
    protected $table = 'state_translation';
    protected $fillable = [		
    							'name',
    							'state_slug',
    							'state_id',
    							'state_title',
    							'locale',
    						];
}
