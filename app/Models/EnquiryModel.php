<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EnquiryModel extends Model
{
    //
    protected $table = "user_enquiry";
    protected $fillable = ['first_name','last_name','email','cmb_country','receive_detail','radiog_dark'];
}
